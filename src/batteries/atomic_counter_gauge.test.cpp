//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/atomic_counter_gauge.hpp>
//
#include <batteries/atomic_counter_gauge.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

using namespace batt::int_types;

using ACG8 = batt::AtomicCounterGauge<u8>;
using ACG16 = batt::AtomicCounterGauge<u16>;
using ACG32 = batt::AtomicCounterGauge<u32>;
using ACG64 = batt::AtomicCounterGauge<u64>;

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
static_assert(ACG8::kNumBits == 8);
static_assert(ACG8::kNumCountBits == 4);
static_assert(ACG8::kAddShift == 0);
static_assert(ACG8::kSubShift == 4);
static_assert(ACG8::kAddOverflowShift == 3);
static_assert(ACG8::kSubOverflowShift == 7);
// clang-format off
static_assert(ACG8::kAddIncrement ==    0b00000001);
static_assert(ACG8::kAddOverflow ==     0b00001000);
static_assert(ACG8::kAddMask ==         0b00001111);
static_assert(ACG8::kSubIncrement ==    0b00010000);
static_assert(ACG8::kSubOverflow ==     0b10000000);
static_assert(ACG8::kSubMask ==         0b11110000);
static_assert(ACG8::kCountMask ==       0b00000111);
static_assert(ACG8::kGaugeMask ==       0b00000111);
// clang-format on

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
static_assert(ACG16::kNumBits == 16);
static_assert(ACG16::kNumCountBits == 8);
static_assert(ACG16::kAddShift == 0);
static_assert(ACG16::kSubShift == 8);
static_assert(ACG16::kAddOverflowShift == 7);
static_assert(ACG16::kSubOverflowShift == 15);
// clang-format off
static_assert(ACG16::kAddIncrement ==    0b0000000000000001);
static_assert(ACG16::kAddOverflow ==     0b0000000010000000);
static_assert(ACG16::kAddMask ==         0b0000000011111111);
static_assert(ACG16::kSubIncrement ==    0b0000000100000000);
static_assert(ACG16::kSubOverflow ==     0b1000000000000000);
static_assert(ACG16::kSubMask ==         0b1111111100000000);
static_assert(ACG16::kCountMask ==       0b0000000001111111);
static_assert(ACG16::kGaugeMask ==       0b0000000001111111);
// clang-format on

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(AtomicCounterGaugeTest, Test)
{
    for (u64 init = 0; init < 65536; ++init) {
        ACG8 x{u8(init)};

        EXPECT_EQ(int(x.load().get_gauge()), int(init & ACG8::kGaugeMask));

        u8 expected_gauge = init & ACG8::kCountMask;
        u8 expected_add_count = expected_gauge;
        u8 expected_sub_count = 0;

        for (int i = 0; i < 10; ++i) {
            while (expected_gauge < ACG8::kGaugeMask) {
                ACG8::State s = x.fetch_add(1);

                EXPECT_EQ(s.get_add_count() & ACG8::kCountMask, expected_add_count & ACG8::kCountMask)
                    << BATT_INSPECT(std::bitset<8>{s.value()});

                EXPECT_EQ(s.get_sub_count() & ACG8::kCountMask, expected_sub_count & ACG8::kCountMask)
                    << BATT_INSPECT(std::bitset<8>{s.value()});

                EXPECT_EQ(s.get_gauge(), expected_gauge) << BATT_INSPECT(std::bitset<8>{s.value()});

                ++expected_gauge;
                ++expected_add_count;
            }

            while (expected_gauge > 0) {
                ACG8::State s = x.fetch_sub(1);

                EXPECT_EQ(s.get_add_count() & ACG8::kCountMask, expected_add_count & ACG8::kCountMask)
                    << BATT_INSPECT(std::bitset<8>{s.value()});

                EXPECT_EQ(s.get_sub_count() & ACG8::kCountMask, expected_sub_count & ACG8::kCountMask)
                    << BATT_INSPECT(std::bitset<8>{s.value()});

                EXPECT_EQ(s.get_gauge(), expected_gauge) << BATT_INSPECT(std::bitset<8>{s.value()});

                --expected_gauge;
                ++expected_sub_count;
            }
        }
    }
}

}  // namespace
