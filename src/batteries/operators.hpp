// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_OPERATORS_HPP
#define BATTERIES_OPERATORS_HPP

#include <batteries/config.hpp>
//
#include <batteries/stream_util.hpp>
#include <batteries/type_traits.hpp>

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/tuple/to_seq.hpp>

#include <tuple>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_UNWRAP(...) __VA_ARGS__

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_TOTALLY_ORDERED(inline_decl, left_type, right_type)                                             \
    BATT_UNWRAP inline_decl bool operator>(const left_type& l, const right_type& r)                          \
    {                                                                                                        \
        return r < l;                                                                                        \
    }                                                                                                        \
    BATT_UNWRAP inline_decl bool operator<=(const left_type& l, const right_type& r)                         \
    {                                                                                                        \
        return !(l > r);                                                                                     \
    }                                                                                                        \
    BATT_UNWRAP inline_decl bool operator>=(const left_type& l, const right_type& r)                         \
    {                                                                                                        \
        return !(l < r);                                                                                     \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_EQUALITY_COMPARABLE(inline_decl, left_type, right_type)                                         \
    BATT_UNWRAP inline_decl bool operator!=(const left_type& l, const right_type& r)                         \
    {                                                                                                        \
        return !(l == r);                                                                                    \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_OBJECT_FIELD_EXPR(r, obj, fieldname) (obj).fieldname

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_OBJECT_FIELD_PARAM(r, obj, fieldname) BATT_OBJECT_FIELD_EXPR(r, obj, fieldname),

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_PRINT_OBJECT_FIELD(r, obj, fieldname)                                                           \
    << " ."                                                                                                  \
    << BOOST_PP_STRINGIZE(fieldname) << "="                                                                  \
                                     << ::batt::make_printable(BATT_OBJECT_FIELD_EXPR(r, obj, fieldname))    \
                                     << ","

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_PRINT_OBJECT_IMPL(type, fields_seq)                                                             \
    std::ostream& operator<<(std::ostream& out, const type& t)                                               \
    {                                                                                                        \
        return out << ::batt::unqualified_name_of<type>()                                                    \
                   << "{" BOOST_PP_SEQ_FOR_EACH(BATT_PRINT_OBJECT_FIELD, (t), fields_seq) << " }";           \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_OBJECT_PRINT_IMPL(inline_decl, type, fields_tuple)                                              \
    BATT_UNWRAP inline_decl BATT_PRINT_OBJECT_IMPL(type, BOOST_PP_TUPLE_TO_SEQ(fields_tuple))

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_EQUALS_OBJECT_IMPL(inline_decl, type, fields_seq)                                               \
    BATT_UNWRAP inline_decl bool operator==(const type& l, const type& r)                                    \
    {                                                                                                        \
        return std::forward_as_tuple(                                                                        \
                   BOOST_PP_SEQ_FOR_EACH(BATT_OBJECT_FIELD_PARAM, (l), fields_seq) true) ==                  \
               std::forward_as_tuple(BOOST_PP_SEQ_FOR_EACH(BATT_OBJECT_FIELD_PARAM, (r), fields_seq) true);  \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_OBJECT_EQUALS_IMPL(inline_decl, type, fields_tuple)                                             \
    BATT_EQUALS_OBJECT_IMPL(inline_decl, type, BOOST_PP_TUPLE_TO_SEQ(fields_tuple))

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_LESS_THAN_OBJECT_IMPL(inline_decl, type, fields_seq)                                            \
    BATT_UNWRAP inline_decl bool operator<(const type& l, const type& r)                                     \
    {                                                                                                        \
        return std::forward_as_tuple(BOOST_PP_SEQ_FOR_EACH(BATT_OBJECT_FIELD_PARAM, (l), fields_seq) true) < \
               std::forward_as_tuple(BOOST_PP_SEQ_FOR_EACH(BATT_OBJECT_FIELD_PARAM, (r), fields_seq) true);  \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
#define BATT_OBJECT_LESS_THAN_IMPL(inline_decl, type, fields_tuple)                                          \
    BATT_LESS_THAN_OBJECT_IMPL(inline_decl, type, BOOST_PP_TUPLE_TO_SEQ(fields_tuple))

}  //namespace batt

#endif  // BATTERIES_OPERATORS_HPP
