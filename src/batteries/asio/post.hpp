//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASIO_POST_HPP
#define BATTERIES_ASIO_POST_HPP

#include <batteries/suppress.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
//
#include <boost/asio/post.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()

#endif  // BATTERIES_ASIO_POST_HPP
