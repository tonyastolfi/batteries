// Copyright 2021-2022 Anthony Paul Astolfi
//
#pragma once

#include <batteries/config.hpp>
//

#ifndef BATT_PLATFORM_IS_WINDOWS
#include <cxxabi.h>
#endif

#include <iterator>
#include <tuple>
#include <type_traits>
#include <typeindex>
#include <utility>
#include <variant>

namespace batt {

// =============================================================================
// IsCallable<Fn, Args...>
//
//  Type alias for std::true_type if `Fn` is callable with `Args...`.
//  Type alias for std::false_type otherwise.
//
namespace detail {

template <typename Fn, typename... Args, typename Result = std::invoke_result_t<Fn, Args...>>
std::true_type is_callable_impl(void*);

template <typename Fn, typename... Args>
std::false_type is_callable_impl(...);

}  // namespace detail

template <typename Fn, typename... Args>
using IsCallable = decltype(detail::is_callable_impl<Fn, Args...>(nullptr));

// =============================================================================
// IsPrintable<T>
//
namespace detail {

template <typename T, typename Result = decltype(std::declval<std::ostream&>() << std::declval<T>())>
std::true_type is_printable_impl(void*);

template <typename Fn, typename... Args>
std::false_type is_printable_impl(...);

}  // namespace detail

template <typename T>
using IsPrintable = decltype(detail::is_printable_impl<T>(nullptr));

// =============================================================================
// IsRange<T>
//
//  Type alias for std::true_type if `T` is a range type.
//  Type alias for std::false_type otherwise.
//
namespace detail {

template <typename T, typename BeginIter = decltype(std::begin(std::declval<T>())),
          typename EndIter = decltype(std::end(std::declval<T>())),
          typename = std::enable_if_t<std::is_same<BeginIter, EndIter>{}>>
std::true_type is_range_impl(void*);

template <typename T>
std::false_type is_range_impl(...);

}  // namespace detail

template <typename T>
using IsRange = decltype(detail::is_range_impl<T>(nullptr));

// =============================================================================
// IsVariant<T>
//
//  Derives std::true_type if `T` is a std::variant type.
//  Derives std::false_type otherwise.
//
template <typename T>
struct IsVariant : std::false_type {
};

template <typename... Ts>
struct IsVariant<std::variant<Ts...>> : std::true_type {
};

// =============================================================================
// IsTuple<T>
//
//  Derives std::true_type if `T` is a std::tuple type.
//  Derives std::false_type otherwise.
//
template <typename T>
struct IsTuple : std::false_type {
};

template <typename... Ts>
struct IsTuple<std::tuple<Ts...>> : std::true_type {
};

// =============================================================================
// StaticType<T>
//
template <typename T>
struct StaticType {
    using type = T;
};

template <typename L, typename R>
inline constexpr bool operator==(StaticType<L>, StaticType<R>)
{
    return std::is_same_v<L, R>;
}
template <typename L, typename R>
inline constexpr bool operator!=(StaticType<L>, StaticType<R>)
{
    return !std::is_same_v<L, R>;
}

static_assert(StaticType<int>{} == StaticType<int>{}, "");
static_assert(StaticType<int>{} != StaticType<unsigned>{}, "");

// =============================================================================
//
template <typename T, T kValue>
struct StaticValue {
    static constexpr T value = kValue;

    constexpr StaticValue() = default;

    constexpr operator T() const
    {
        return value;
    }
};

#define BATT_STATIC_VALUE(expr) ::batt::StaticValue<decltype(expr), (expr)>

// =============================================================================
// Enables/disables a constructor template when the argments do not cause it to
// shadow a built-in method.
//
// Usage:
//
// ```
// class MyType {
// public:
//   template <typename... Args, typename = batt::EnableIfNoShadow<MyType, Args...>>
//   MyType(Args&&... args) { /* something other than copy/move/default */ }
// };
// ```
//
template <typename T, typename... Args>
using EnableIfNoShadow =
    std::enable_if_t<!std::is_same<std::tuple<std::decay_t<T>*>, std::tuple<std::decay_t<Args>*...>>{}
                     // Copy or move ctor
                     && !std::is_same<std::tuple<>, std::tuple<std::decay_t<Args>*...>>{}  // Default ctor
                     >;

// =============================================================================
/** \brief Returns true iff any of the passed arguments evaluates to true (base case; always returns false).
 */
inline constexpr bool is_any_true() noexcept
{
    return false;
}

/** \brief Returns true iff any of the passed arguments evaluates to true (recursive case; short circuits if
 * `first` is true.).
 */
template <typename... Rest>
inline constexpr bool is_any_true(bool first, Rest&&... rest) noexcept
{
    return first || is_any_true(std::forward<Rest>(rest)...);
}

// =============================================================================
/** \brief Returns true iff all of the passed arguments evaluate to true (base case; always returns false).
 */
inline constexpr bool are_all_true() noexcept
{
    return true;
}

/** \brief Returns true iff any of the passed arguments evaluates to true (recursive case; short circuits if
 * `first` is true.).
 */
template <typename... Rest>
inline constexpr bool are_all_true(bool first, Rest&&... rest) noexcept
{
    return first && are_all_true(std::forward<Rest>(rest)...);
}

// =============================================================================
/** \brief Trait that evaluates to true iff `T` is the same as one of the MatchTs.
 */
template <typename T, typename... MatchTs>
inline constexpr bool IsOneOf = is_any_true(std::is_same_v<T, MatchTs>...);

/** \brief Trait that evaluates to true iff `T` decays to one of the MatchTs.
 */
template <typename T, typename... MatchTs>
inline constexpr bool DecaysToOneOf = IsOneOf<std::decay_t<T>, MatchTs...>;

// =============================================================================
// Decays T iff it is an rvalue reference type.
//
template <typename T>
struct DecayRValueRefImpl
    : std::conditional_t<  //
          /*if */ std::is_rvalue_reference_v<T>,
          /* then */ std::decay<T>,
          /* else */ batt::StaticType<T>> {
};

template <typename T>
using DecayRValueRef = typename DecayRValueRefImpl<T>::type;

// =============================================================================
// CanBeEqCompared<T, U> - std::true_type or std::false_type depending on whether types `T` and `U` can be
// equality-compared.
//
namespace detail {

template <typename T, typename U, typename = decltype(std::declval<const T&>() == std::declval<const U&>())>
std::true_type can_be_eq_compared_helper(const T*, const U*)
{
    return {};
}

template <typename T, typename U>
std::false_type can_be_eq_compared_helper(...)
{
    return {};
}

}  // namespace detail

template <typename T, typename U = T>
using CanBeEqCompared = decltype(detail::can_be_eq_compared_helper<T, U>(nullptr, nullptr));

// =============================================================================

/*! \brief Calculates and returns the demangled name of the given type as a null-terminated C-string.
 */
inline const char* name_of(const std::type_index& index)
{
    int status = -1;
#ifdef BATT_PLATFORM_IS_WINDOWS
    return index.name();
#else
    return abi::__cxa_demangle(index.name(), NULL, NULL, &status);
#endif
}

/*! \brief Calculates and returns the demangled name of the given type as a null-terminated C-string.
 */
inline const char* name_of(const std::type_info& info)
{
    return name_of(std::type_index{info});
}

/*! \brief Calculates and returns the demangled name of the given type as a null-terminated C-string.
 */
template <typename T>
const char* name_of(batt::StaticType<T> = {})
{
    return name_of(typeid(T));
}

/** \brief Calculates and returns the demangled name of the given type, without the namespace qualifier.
 */
template <typename T>
const char* unqualified_name_of(batt::StaticType<T> = {})
{
    static const char* cached_ = [] {
        const char* name_str = name_of(batt::StaticType<T>{});
        const char* result = name_str;

        bool prev_colon = false;
        while (name_str && *name_str) {
            if (*name_str == ':') {
                if (prev_colon) {
                    result = name_str + 1;
                }
                prev_colon = true;
            } else {
                prev_colon = false;
            }
            ++name_str;
        }

        return result;
    }();

    return cached_;
}

// =============================================================================

namespace detail {

template <typename T>
struct EmptyBaseHelper : T {
    char bytes[16];
};

struct EmptyBaseTestCase {
};

struct EmptyBaseTestCase2 {
    char ch;
};

template <typename T,  //
          typename = std::enable_if_t<sizeof(detail::EmptyBaseHelper<T>) == 16>>
inline constexpr std::true_type class_can_be_empty_base(StaticType<T> = {})
{
    return {};
}

template <typename T,                                                             //
          typename = std::enable_if_t<sizeof(detail::EmptyBaseHelper<T>) != 16>,  //
          typename = void>
inline constexpr std::false_type class_can_be_empty_base(StaticType<T> = {})
{
    return {};
}

}  //namespace detail

/** \brief Returns true iff the specified type can be optimized to zero size via empty base class
 * optimization.
 */
template <typename T,  //
          typename = std::enable_if_t<std::is_class_v<T>>>
inline constexpr auto can_be_empty_base(StaticType<T> = {})
{
    return detail::class_can_be_empty_base<T>();
}

template <typename T,                                        //
          typename = std::enable_if_t<!std::is_class_v<T>>,  //
          typename = void>
inline constexpr std::false_type can_be_empty_base(StaticType<T> = {})
{
    return {};
}

//+++++++++++-+-+--+----- --- -- -  -  -   -

static_assert(can_be_empty_base<detail::EmptyBaseTestCase>());
static_assert(!can_be_empty_base<detail::EmptyBaseTestCase2>());
static_assert(!can_be_empty_base<char>());
static_assert(!can_be_empty_base<int>());

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

/** \brief Type trait that maps subtractable type `T` to the difference type.
 */
template <typename T>
struct Difference : StaticType<decltype(std::declval<T>() - std::declval<T>())> {
};

template <>
struct Difference<unsigned char> : StaticType<char> {
};

template <>
struct Difference<unsigned short> : StaticType<short> {
};

template <>
struct Difference<unsigned long> : StaticType<long> {
};

template <>
struct Difference<unsigned long long> : StaticType<long long> {
};

template <typename A, typename B>
struct Difference<std::pair<A, B>>
    : StaticType<std::pair<typename Difference<A>::type, typename Difference<B>::type>> {
};

template <typename... Ts>
struct Difference<std::tuple<Ts...>> : StaticType<std::tuple<typename Difference<Ts>::type...>> {
};

}  // namespace batt
