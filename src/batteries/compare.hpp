//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_COMPARE_HPP
#define BATTERIES_COMPARE_HPP

#include <batteries/config.hpp>
//
#include <batteries/bounds.hpp>
#include <batteries/endian.hpp>
#include <batteries/hint.hpp>
#include <batteries/int_types.hpp>

#include <algorithm>
#include <cstring>
#include <ostream>
#include <string_view>

namespace batt {

enum struct Order : i8 {
    Less = -1,
    Equal = 0,
    Greater = 1,
};

inline std::ostream& operator<<(std::ostream& out, Order t)
{
    switch (t) {
    case Order::Less:
        return out << "Less";
    case Order::Equal:
        return out << "Equal";
    case Order::Greater:
        return out << "Greater";
    default:
        break;
    }
    return out << "BAD batt::OrderValue (" << (int)t << ")!!!";
}

inline Order compare(const std::string_view& a, const std::string_view& b) noexcept
{
    const usize len = std::min(a.size(), b.size());
    const auto result = std::memcmp(a.data(), b.data(), len);
    if (result == 0) {
        if (a.size() < b.size()) {
            return Order::Less;
        }
        if (a.size() == b.size()) {
            return Order::Equal;
        }
        return Order::Greater;
    } else if (result < 0) {
        return Order::Less;
    } else {
        return Order::Greater;
    }
}

}  //namespace batt

#endif  // BATTERIES_COMPARE_HPP
