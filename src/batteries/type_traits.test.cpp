// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/type_traits.hpp>
//

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <array>
#include <batteries/type_traits.hpp>
#include <map>
#include <string>
#include <vector>

// These tests are mostly compile-time.
//

// =============================================================================
// IsCallable tests.
//
using FunctionPointer = void (*)(int, int);

static_assert(batt::IsCallable<FunctionPointer, char, long>{}, "IsCallableOk");

static_assert(!batt::IsCallable<FunctionPointer, char, long, long>{}, "IsCallableTooManyArgs");
static_assert(!batt::IsCallable<FunctionPointer, char>{}, "IsCallableTooFewArgs");

// =============================================================================
// IsRange tests.
//
struct NotARange {
};

struct IsARange {
    const char* str_ = "abc";

    const char* begin() const
    {
        return str_;
    }
    const char* end() const
    {
        return str_ + 3;
    }
};

static_assert(batt::IsRange<std::string>{}, "IsRangeString");
static_assert(batt::IsRange<std::vector<int>>{}, "IsRangeVector");
static_assert(batt::IsRange<std::map<int, int>>{}, "IsRangeMap");
static_assert(batt::IsRange<std::array<int, 4>>{}, "IsRangeArray");
static_assert(batt::IsRange<IsARange>{}, "IsRangeCustomType");

static_assert(!batt::IsRange<NotARange>{}, "IsRangeCustomTypeFail");
static_assert(!batt::IsRange<int>{}, "IsRangeInt");
static_assert(!batt::IsRange<std::array<int, 4>*>{}, "IsRangePointer");

// =============================================================================
// IsVariant tests.
//
static_assert(batt::IsVariant<std::variant<int, double>>{}, "IsVariantTrue");
static_assert(!batt::IsVariant<const std::variant<int, double>>{}, "IsVariantFalse1");
static_assert(!batt::IsVariant<std::tuple<int, double>>{}, "IsVariantFalse2");

// =============================================================================
// IsTuple tests.
//
static_assert(batt::IsTuple<std::tuple<int, double>>{}, "IsTupleTrue");
static_assert(!batt::IsTuple<const std::tuple<int, double>>{}, "IsTupleFalse1");
static_assert(!batt::IsTuple<std::variant<int, double>>{}, "IsTupleFalse1");

// =============================================================================
// StaticValue tests.
//
static_assert(BATT_STATIC_VALUE(&IsARange::begin){} == &IsARange::begin);

// =============================================================================
// DecayRValueRef tests.
//
static_assert(std::is_same_v<batt::DecayRValueRef<int>, int>);
static_assert(std::is_same_v<batt::DecayRValueRef<const int>, const int>);
static_assert(std::is_same_v<batt::DecayRValueRef<int&>, int&>);
static_assert(std::is_same_v<batt::DecayRValueRef<const int&>, const int&>);
static_assert(std::is_same_v<batt::DecayRValueRef<int&&>, int>);
static_assert(std::is_same_v<batt::DecayRValueRef<const int&&>, int>);

// =============================================================================
// CanBeEqCompared tests.
//
static_assert(batt::CanBeEqCompared<int, long>{});
static_assert(batt::CanBeEqCompared<std::string, const char*>{});
static_assert(batt::CanBeEqCompared<const char*, std::string>{});
static_assert(!batt::CanBeEqCompared<bool, std::string>{});
static_assert(!batt::CanBeEqCompared<NotARange, NotARange>{});
static_assert(!batt::CanBeEqCompared<NotARange>{});

TEST(TypeTraitsTest, NameOf)
{
    const auto verify_case = [&](auto static_type, const auto& matcher) {
        using T = typename decltype(static_type)::type;

        EXPECT_THAT(batt::name_of<T>(), matcher);
        EXPECT_THAT(batt::name_of(batt::StaticType<T>{}), matcher);
        EXPECT_THAT(batt::name_of(typeid(T)), matcher);
        EXPECT_THAT(batt::name_of(std::type_index{typeid(T)}), matcher);
    };

    verify_case(batt::StaticType<int>{}, ::testing::StrEq("int"));
    verify_case(batt::StaticType<char>{}, ::testing::StrEq("char"));
    verify_case(batt::StaticType<long>{}, ::testing::StrEq("long"));
    verify_case(batt::StaticType<float>{}, ::testing::StrEq("float"));

#if BATT_COMPILER_IS_MSVC
    verify_case(batt::StaticType<const char*>{}, ::testing::StrEq("char const * __ptr64"));
    verify_case(batt::StaticType<void (*)(int, int)>{}, ::testing::StrEq("void (__cdecl*)(int,int)"));
    verify_case(
        batt::StaticType<std::map<std::string, std::vector<int>>>{},
        ::testing::StrEq("class std::map<class std::basic_string<char,struct std::char_traits<char>,class "
                         "std::allocator<char> >,class std::vector<int,class std::allocator<int> >,struct "
                         "std::less<class std::basic_string<char,struct std::char_traits<char>,class "
                         "std::allocator<char> > >,class std::allocator<struct std::pair<class "
                         "std::basic_string<char,struct std::char_traits<char>,class std::allocator<char> > "
                         "const ,class std::vector<int,class std::allocator<int> > > > >"));
#else
    verify_case(batt::StaticType<const char*>{}, ::testing::StrEq("char const*"));
    verify_case(batt::StaticType<void (*)(int, int)>{}, ::testing::StrEq("void (*)(int, int)"));
    verify_case(batt::StaticType<std::map<std::string, std::vector<int>>>{},
                ::testing::MatchesRegex("std::.*map<std::.*string.*, std::.*vector<int.*>.*>"));
#endif
}

// =============================================================================
// is_any_true tests.
//
static_assert(batt::is_any_true() == false);
static_assert(batt::is_any_true(false) == false);
static_assert(batt::is_any_true(true) == true);
static_assert(batt::is_any_true(true, false) == true);
static_assert(batt::is_any_true(true, true) == true);
static_assert(batt::is_any_true(false, true) == true);
static_assert(batt::is_any_true(false, false) == false);
static_assert(batt::is_any_true(false, false, true, false, true) == true);
static_assert(batt::is_any_true(false, false, false, false, false) == false);

// =============================================================================
// are_all_true tests.
//
static_assert(batt::are_all_true() == true);
static_assert(batt::are_all_true(false) == false);
static_assert(batt::are_all_true(true) == true);
static_assert(batt::are_all_true(true, false) == false);
static_assert(batt::are_all_true(true, true) == true);
static_assert(batt::are_all_true(false, true) == false);
static_assert(batt::are_all_true(false, false) == false);
static_assert(batt::are_all_true(true, true, true, false, true) == false);
static_assert(batt::are_all_true(true, true, true, true, true) == true);

// =============================================================================
// IsOneOf tests.
//
static_assert(batt::IsOneOf<int, char, int, float> == true);
static_assert(batt::IsOneOf<int*, char, int, float> == false);
static_assert(batt::IsOneOf<int*> == false);
static_assert(batt::IsOneOf<int*, int*> == true);

// =============================================================================
// DecaysToOneOf tests.
//
static_assert(batt::DecaysToOneOf<int, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int*, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int*> == false);
static_assert(batt::DecaysToOneOf<int*, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile> == false);
static_assert(batt::DecaysToOneOf<int* volatile, int*> == true);

static_assert(batt::DecaysToOneOf<int const, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* const, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* const> == false);
static_assert(batt::DecaysToOneOf<int* const, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile const, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile const, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile const> == false);
static_assert(batt::DecaysToOneOf<int* volatile const, int*> == true);

static_assert(batt::DecaysToOneOf<int const&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* const&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* const&> == false);
static_assert(batt::DecaysToOneOf<int* const&, int*> == true);

static_assert(batt::DecaysToOneOf<int&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int*&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int*&> == false);
static_assert(batt::DecaysToOneOf<int*&, int*> == true);

static_assert(batt::DecaysToOneOf<int const&&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* const&&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* const&&> == false);
static_assert(batt::DecaysToOneOf<int* const&&, int*> == true);

static_assert(batt::DecaysToOneOf<int&&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int*&&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int*&&> == false);
static_assert(batt::DecaysToOneOf<int*&&, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile const&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile const&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile const&> == false);
static_assert(batt::DecaysToOneOf<int* volatile const&, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile&> == false);
static_assert(batt::DecaysToOneOf<int* volatile&, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile const&&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile const&&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile const&&> == false);
static_assert(batt::DecaysToOneOf<int* volatile const&&, int*> == true);

static_assert(batt::DecaysToOneOf<int volatile&&, char, int, float> == true);
static_assert(batt::DecaysToOneOf<int* volatile&&, char, int, float> == false);
static_assert(batt::DecaysToOneOf<int* volatile&&> == false);
static_assert(batt::DecaysToOneOf<int* volatile&&, int*> == true);
