//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#include <batteries/instance_counter.hpp>
//
#include <batteries/instance_counter.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

struct A : batt::InstanceCounter<A> {
};

TEST(InstanceCounterTest, Test)
{
    EXPECT_EQ(sizeof(A), 1);
    EXPECT_EQ(batt::InstanceCounter<A>::get(), 0);
    {
        A a1;
        EXPECT_EQ(batt::InstanceCounter<A>::get(), 1);
        {
            A a2;
            EXPECT_EQ(batt::InstanceCounter<A>::get(), 2);
            {
                A a3;
                EXPECT_EQ(batt::InstanceCounter<A>::get(), 3);
            }
            EXPECT_EQ(batt::InstanceCounter<A>::get(), 2);
        }
        EXPECT_EQ(batt::InstanceCounter<A>::get(), 1);
    }
    EXPECT_EQ(batt::InstanceCounter<A>::get(), 0);
}

}  // namespace
