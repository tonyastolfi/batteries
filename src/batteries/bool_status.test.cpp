//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/bool_status.hpp>
//
#include <batteries/bool_status.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/stream_util.hpp>
#include <batteries/utility.hpp>

namespace {

TEST(BoolStatusTest, Test)
{
    EXPECT_EQ(batt::make_default<batt::BoolStatus>(), batt::BoolStatus::kFalse);
    EXPECT_EQ(batt::bool_status_from(false), batt::BoolStatus::kFalse);
    EXPECT_EQ(batt::bool_status_from(true), batt::BoolStatus::kTrue);

    {
        constexpr batt::BoolStatus kUnknown = batt::BoolStatus::kUnknown;
        constexpr batt::BoolStatus kFalse = batt::BoolStatus::kFalse;
        constexpr batt::BoolStatus kTrue = batt::BoolStatus::kTrue;

        EXPECT_EQ(kUnknown || kUnknown, kUnknown);
        EXPECT_EQ(kUnknown || kFalse, kUnknown);
        EXPECT_EQ(kUnknown || kTrue, kTrue);
        EXPECT_EQ(kFalse || kUnknown, kUnknown);
        EXPECT_EQ(kFalse || kFalse, kFalse);
        EXPECT_EQ(kFalse || kTrue, kTrue);
        EXPECT_EQ(kTrue || kUnknown, kTrue);
        EXPECT_EQ(kTrue || kFalse, kTrue);
        EXPECT_EQ(kTrue || kTrue, kTrue);

        EXPECT_EQ(kUnknown && kUnknown, kUnknown);
        EXPECT_EQ(kUnknown && kFalse, kFalse);
        EXPECT_EQ(kUnknown && kTrue, kUnknown);
        EXPECT_EQ(kFalse && kUnknown, kFalse);
        EXPECT_EQ(kFalse && kFalse, kFalse);
        EXPECT_EQ(kFalse && kTrue, kFalse);
        EXPECT_EQ(kTrue && kUnknown, kUnknown);
        EXPECT_EQ(kTrue && kFalse, kFalse);
        EXPECT_EQ(kTrue && kTrue, kTrue);

        EXPECT_EQ(!kUnknown, kUnknown);
        EXPECT_EQ(!kFalse, kTrue);
        EXPECT_EQ(!kTrue, kFalse);

        EXPECT_THAT(batt::to_string(kUnknown), ::testing::StrEq("Unknown"));
        EXPECT_THAT(batt::to_string(kFalse), ::testing::StrEq("False"));
        EXPECT_THAT(batt::to_string(kTrue), ::testing::StrEq("True"));
    }
}

}  // namespace
