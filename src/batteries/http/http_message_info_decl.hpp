//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_MESSAGE_INFO_DECL_HPP
#define BATTERIES_HTTP_HTTP_MESSAGE_INFO_DECL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/stream_buffer.hpp>
#include <batteries/http/http_data.hpp>
#include <batteries/operators.hpp>
#include <batteries/optional.hpp>
#include <batteries/pico_http/parser.hpp>
#include <batteries/small_vec.hpp>

namespace batt {

/** \brief A summary of information about a response message that is necessary for the connection to
 * correctly handle it.
 */
struct HttpMessageInfo {
    explicit HttpMessageInfo(const pico_http::Response& response) noexcept;
    explicit HttpMessageInfo(const pico_http::Request& request) noexcept;

    HttpMessageInfo(int major_version, int minor_version,
                    const batt::SmallVecBase<pico_http::MessageHeader>& headers) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    bool is_valid() const
    {
        return this->content_length || this->chunked_encoding || !this->keep_alive;
    }

    HttpData get_data(StreamBuffer& input_buffer);

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Set from the `Content-Length` header, if present.
     */
    Optional<usize> content_length;

    /** \brief Set to true if `Connection: keep-alive` was set, or if the HTTP version is 1.1 or later and
     * there is no explicit `Connection: close` header.
     */
    bool keep_alive;

    /** \brief Set to true if `Transfer-Encoding: chunked` is present.
     */
    bool chunked_encoding;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

BATT_OBJECT_PRINT_IMPL((inline), HttpMessageInfo, (content_length, keep_alive, chunked_encoding));

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_MESSAGE_INFO_DECL_HPP
