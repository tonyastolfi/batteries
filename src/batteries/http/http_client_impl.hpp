//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_CLIENT_IMPL_HPP
#define BATTERIES_HTTP_CLIENT_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/http/http_client_host_context.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpClient::HttpClient(boost::asio::io_context& io) noexcept
    : io_{io}
    , default_max_connections_per_host_{HttpClientHostContext::kDefaultMaxConnections}
    , default_connection_timeout_ms_{HttpClientHostContext::kDefaultIdleConnectionTimeoutMs}
    , host_contexts_{}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status HttpClient::submit_request(const HostAddress& host_address,
                                                   Pin<HttpRequest>&& request, Pin<HttpResponse>&& response)
{
    BATT_CHECK_NOT_NULLPTR(request);
    BATT_CHECK_NOT_NULLPTR(response);

    SharedPtr<HttpClientHostContext> host_context = this->find_host_context(host_address);
    return host_context->submit_request(std::move(request), std::move(response));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize HttpClient::get_default_max_connections_per_host() const noexcept
{
    return this->default_max_connections_per_host_.load();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClient::set_default_max_connections_per_host(usize max_connections) noexcept
{
    this->default_max_connections_per_host_.store(max_connections);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClient::set_max_connections_per_host(const HostAddress& host_address,
                                                               usize max_connections) noexcept
{
    SharedPtr<HttpClientHostContext> host_context = this->find_host_context(host_address);
    host_context->set_max_connections(max_connections);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL i32 HttpClient::get_default_connection_timeout_ms() const noexcept
{
    return this->default_connection_timeout_ms_.load();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void HttpClient::set_default_connection_timeout_ms(i32 timeout_ms) noexcept
{
    this->default_connection_timeout_ms_.store(timeout_ms);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize HttpClient::count_active_connections_for_host(const HostAddress& host_address) noexcept
{
    SharedPtr<HttpClientHostContext> host_context =
        this->find_host_context(host_address, CreateIfNotFound{false});

    if (host_context == nullptr) {
        return 0;
    }

    return host_context->count_active_connections();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize HttpClient::count_active_connections() const noexcept
{
    usize total = 0;

    BATT_SCOPED_CONST_LOCK(locked_contexts, this->host_contexts_);

    for (auto& [host_addr, p_context] : *locked_contexts) {
        BATT_CHECK_NOT_NULLPTR(p_context) << BATT_INSPECT(host_addr);
        total += p_context->count_active_connections();
    }

    return total;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL SharedPtr<HttpClientHostContext> HttpClient::find_host_context(
    const HostAddress& host_address, CreateIfNotFound create)
{
    BATT_SCOPED_LOCK(locked_contexts, this->host_contexts_);

    auto iter = locked_contexts->find(host_address);
    if (iter == locked_contexts->end()) {
        if (!create) {
            return nullptr;
        }
        iter = locked_contexts
                   ->emplace(host_address, make_shared<HttpClientHostContext>(/*client=*/*this, host_address))
                   .first;
    }
    return iter->second;
}

}  // namespace batt

#endif  // BATTERIES_HTTP_CLIENT_IMPL_HPP
