//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/http/http_client_connection_decl.hpp>

#include <batteries/http/http_client_connection.ipp>

#if BATT_HEADER_ONLY
#include <batteries/http/http_client_connection_impl.hpp>
#endif  // BATT_HEADER_ONLY
