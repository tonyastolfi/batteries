//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_HTTP_MESSAGE_INFO_IMPL_HPP
#define BATTERIES_HTTP_HTTP_MESSAGE_INFO_IMPL_HPP

#include <batteries/http/http_message_info_decl.hpp>

#include <batteries/http/http_chunk_decoder.hpp>
#include <batteries/http/http_data.hpp>
#include <batteries/http/http_header.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpMessageInfo::HttpMessageInfo(const pico_http::Response& response) noexcept
    : HttpMessageInfo{response.major_version, response.minor_version, response.headers}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ HttpMessageInfo::HttpMessageInfo(const pico_http::Request& request) noexcept
    : HttpMessageInfo{request.major_version, request.minor_version, request.headers}
{
    if (request.method != "PUT" && request.method != "POST") {
        this->content_length = 0;
        this->chunked_encoding = false;
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL
HttpMessageInfo::HttpMessageInfo(int major_version, int minor_version,
                                 const batt::SmallVecBase<pico_http::MessageHeader>& headers) noexcept
    : content_length{find_header(headers, "Content-Length").flat_map([](std::string_view s) {
        return Optional{from_string<usize>(std::string(s))};
    })}
    , keep_alive{find_header(headers, "Connection")
                     .map([](std::string_view s) {
                         return s == "keep-alive";
                     })
                     .value_or(major_version == 1 && minor_version >= 1)}
    , chunked_encoding{find_header(headers, "Transfer-Encoding")
                           .map([](std::string_view s) {
                               return s == "chunked";
                           })
                           .value_or(false)}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL HttpData HttpMessageInfo::get_data(StreamBuffer& input_buffer)
{
    return HttpData{[&]() -> BufferSource {
        const auto consume_trailer = IncludeHttpTrailer{true};

        if (this->content_length == None) {
            if (this->chunked_encoding) {
                return HttpChunkDecoder<StreamBuffer&>{input_buffer, consume_trailer};
            } else {
                if (this->keep_alive) {
                    return BufferSource{};
                } else {
                    return std::ref(input_buffer);
                }
            }
        } else {
            if (this->chunked_encoding) {
                return HttpChunkDecoder<StreamBuffer&>{input_buffer, consume_trailer}  //
                       | seq::take_n(*this->content_length);
            } else {
                return input_buffer | seq::take_n(*this->content_length);
            }
        }
    }()};
}

}  //namespace batt

#endif  // BATTERIES_HTTP_HTTP_MESSAGE_INFO_IMPL_HPP
