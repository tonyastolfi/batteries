//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_HTTP_SSL_TRANSPORT_IMPL_HPP
#define BATTERIES_HTTP_SSL_TRANSPORT_IMPL_HPP

#include <atomic>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ SslTransport::SslTransport(HttpClientHostContext& host_context) noexcept
    : io_{host_context.get_io_context()}
    , stream_id_{[] {
        static std::atomic<i64> next_id{0};
        return next_id.fetch_add(1) + 1;
    }()}
    , close_initiated_{false}
    , ssl_context_{}
    , ssl_stream_{this->io_,
                  [&]() -> boost::asio::ssl::context& {
                      host_context.get_ssl_init_fn()(this->ssl_context_, host_context.host_address());
                      BATT_CHECK(this->ssl_context_);
                      return *this->ssl_context_;
                  }()}
    , socket_{this->ssl_stream_.lowest_layer()}
    , strand_{this->io_.get_executor()}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL boost::asio::ip::tcp::endpoint SslTransport::local_endpoint(ErrorCode& ec) const noexcept
{
    return Task::await<boost::asio::ip::tcp::endpoint>([&](auto&& handler) {
        boost::asio::dispatch(this->strand_,
                              bind_handler(BATT_FORWARD(handler), [this, &ec](auto&& handler) mutable {
                                  //----- --- -- -  -  -   -
                                  // On strand.
                                  //
                                  // It is ok to call the handler directly (instead of posting it using
                                  // its associated executor), since we know it comes directly from
                                  // Task::await.
                                  //
                                  BATT_FORWARD(handler)(this->socket_.local_endpoint(ec));
                              }));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL boost::asio::ip::tcp::endpoint SslTransport::remote_endpoint(ErrorCode& ec) const noexcept
{
    return Task::await<boost::asio::ip::tcp::endpoint>([&](auto&& handler) {
        boost::asio::dispatch(this->strand_,
                              bind_handler(BATT_FORWARD(handler), [this, &ec](auto&& handler) mutable {
                                  //----- --- -- -  -  -   -
                                  // On strand.
                                  //
                                  // It is ok to call the handler directly (instead of posting it using
                                  // its associated executor), since we know it comes directly from
                                  // Task::await.
                                  //
                                  BATT_FORWARD(handler)(this->socket_.remote_endpoint(ec));
                              }));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool SslTransport::is_open() const noexcept
{
    if (this->close_initiated_.load() == true) {
        return false;
    }

    return Task::await<bool>([&](auto&& handler) {
        boost::asio::dispatch(this->strand_,
                              bind_handler(BATT_FORWARD(handler), [this](auto&& handler) mutable {
                                  //----- --- -- -  -  -   -
                                  // On strand.
                                  //
                                  // It is ok to call the handler directly (instead of posting it using
                                  // its associated executor), since we know it comes directly from
                                  // Task::await.
                                  //
                                  bool result = this->socket_.is_open();
                                  BATT_FORWARD(handler)(result);
                              }));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void SslTransport::close(ErrorCode& ec) noexcept
{
    if (this->close_initiated_.exchange(true)) {
        ec = ErrorCode{};
        return;
    }

    ec = Task::await<ErrorCode>([this](auto&& handler) {
        boost::asio::dispatch(
            this->strand_, bind_handler(BATT_FORWARD(handler), [this](auto&& handler) mutable {
                //----- --- -- -  -  -   -
                // On strand.
                //
                this->ssl_stream_.async_shutdown(bind_executor(
                    this->strand_, bind_handler(handler, [this](auto&& handler, const ErrorCode& ssl_ec) {
                        // On strand.
                        //
                        // Regardless how the SSL shutdown went, close the socket.
                        //
                        ErrorCode tcp_ec;
                        this->socket_.close(tcp_ec);

                        // If multiple errors occurred, report the one at the SSL level.
                        //
                        const ErrorCode ec = [&] {
                            if (ssl_ec && ssl_ec != boost::asio::ssl::error::stream_truncated) {
                                return ssl_ec;
                            }
                            return tcp_ec;
                        }();

                        // It is ok to call the handler directly (instead of posting it using
                        // its associated executor), since we know it comes directly from
                        // Task::await.
                        //
                        BATT_FORWARD(handler)(ec);
                    })));
            }));
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void SslTransport::shutdown(boost::asio::socket_base::shutdown_type mode,
                                             ErrorCode& ec) noexcept
{
    ec = Task::await<ErrorCode>([this, mode](auto&& handler) {
        boost::asio::dispatch(this->strand_,
                              bind_handler(BATT_FORWARD(handler), [this, mode](auto&& handler) mutable {
                                  //----- --- -- -  -  -   -
                                  // On strand.
                                  //
                                  // It is ok to call the handler directly (instead of posting it using
                                  // its associated executor), since we know it comes directly from
                                  // Task::await.
                                  //
                                  ErrorCode ec;
                                  this->socket_.shutdown(mode, ec);
                                  BATT_FORWARD(handler)(ec);
                              }));
    });
}

}  //namespace batt

#endif  // BATTERIES_HTTP_SSL_TRANSPORT_IMPL_HPP
