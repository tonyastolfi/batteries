//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/http/http_message_info.hpp>
//
#include <batteries/http/http_message_info_impl.hpp>

#endif  // !BATT_HEADER_ONLY
