//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/exception.hpp>
//
#include <batteries/exception.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iomanip>

namespace {

// Test Plan:
//  1. Examples in exception.hpp
//     1. BATT_THROW_IF_NOT_OK
//        1. is ok
//        2. not ok
//     2. BATT_OK_RESULT_OR_THROW
//        1. is ok
//        2. not ok
//  2. BATT_THROW_IF_NOT_OK with extra info `<<`-inserted should not even evaluate the extra info expressions
//     when ok is true
//  3. Verify that BATT_THROW_IF_NOT_OK can accept different types of `<<`-inserted values, include io manip
//     functions like std::endl, std::hex, etc.
//

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  1. Examples in exception.hpp
//     1. BATT_THROW_IF_NOT_OK
//        1. is ok
//
TEST(ExceptionTest, ThrowIfNotOkSuccess)
{
    EXPECT_NO_THROW({
        batt::Status s1;

        EXPECT_TRUE(s1.ok());

        BATT_THROW_IF_NOT_OK(s1) << "A message that should never be seen";
    });

    EXPECT_NO_THROW({
        batt::StatusOr<int> s1 = 4;

        ASSERT_TRUE(s1.ok());
        EXPECT_EQ(*s1, 4);

        BATT_THROW_IF_NOT_OK(s1) << "A message that should never be seen";
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  1. Examples in exception.hpp
//     1. BATT_THROW_IF_NOT_OK
//        2. not ok
//
TEST(ExceptionTest, ThrowIfNotOkFailure)
{
    const char* extra_message = "Some extra message info";

    EXPECT_THROW(
        {
            batt::Status s1 = batt::StatusCode::kNotFound;

            try {
                BATT_THROW_IF_NOT_OK(s1) << extra_message;

                FAIL();

            } catch (batt::StatusException& e) {
                EXPECT_THAT(e.what(), ::testing::HasSubstr(extra_message));
                EXPECT_THAT(e.what(), ::testing::HasSubstr(batt::to_string(s1)));
                throw;
            }
        },
        batt::StatusException);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  1. Examples in exception.hpp
//     2. BATT_OK_RESULT_OR_THROW
//        1. is ok
//
TEST(ExceptionTest, OkResultOrThrowSuccess)
{
    batt::StatusOr<int> a = 5, b = 6, c = 7;

    EXPECT_NO_THROW({
        const int d = BATT_OK_RESULT_OR_THROW(a) + BATT_OK_RESULT_OR_THROW(b) + BATT_OK_RESULT_OR_THROW(c);

        EXPECT_EQ(d, 18);
    });
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  1. Examples in exception.hpp
//     2. BATT_OK_RESULT_OR_THROW
//        2. not ok
//
TEST(ExceptionTest, OkResultOrThrowFailure)
{
    batt::StatusOr<int> a = 5, b = 6, c;
    EXPECT_THROW(
        {
            try {
                [[maybe_unused]] const int d =
                    BATT_OK_RESULT_OR_THROW(a) + BATT_OK_RESULT_OR_THROW(b) + BATT_OK_RESULT_OR_THROW(c);

                FAIL();

            } catch (batt::StatusException& e) {
                EXPECT_THAT(e.what(),
                            ::testing::HasSubstr(batt::to_string(batt::Status{batt::StatusCode::kUnknown})));
                throw;
            }
        },
        batt::StatusException);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  2. BATT_THROW_IF_NOT_OK with extra info `<<`-inserted should not even evaluate the extra info expressions
//     when ok is true
//
TEST(ExceptionTest, NoEvalWhenOk)
{
    bool evaluated = false;

    batt::Status s1;

    EXPECT_NO_THROW({
        BATT_THROW_IF_NOT_OK(s1) << [&] {
            evaluated = true;
            return "This is a problem!";
        }();
    });

    EXPECT_FALSE(evaluated);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
//  3. Verify that BATT_THROW_IF_NOT_OK can accept different types of `<<`-inserted values, include io manip
//     functions like std::endl, std::hex, etc.
//
TEST(ExceptionTest, ExtraInfoPrint)
{
    EXPECT_THROW(
        {
            batt::Status s1 = batt::StatusCode::kInternal;

            try {
                BATT_THROW_IF_NOT_OK(s1) << "Circular fraction" << std::endl
                                         << std::setprecision(6) << 3.14159 << std::endl
                                         << "Irrational gem";

                FAIL();

            } catch (batt::StatusException& e) {
                EXPECT_THAT(e.what(), ::testing::HasSubstr("Circular fraction\n3.14159\nIrrational gem"));
                throw;
            }
        },
        batt::StatusException);
}

}  // namespace
