//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/async/latch_decl.hpp>

#include <batteries/config.hpp>

#include <batteries/async/watch.ipp>

#include <batteries/async/latch_impl.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/latch_impl.hpp>
#endif
