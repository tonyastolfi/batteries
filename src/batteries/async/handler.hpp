//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_HANDLER_HPP
#define BATTERIES_ASYNC_HANDLER_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/buffer.hpp>
#include <batteries/int_types.hpp>
#include <batteries/static_assert.hpp>
#include <batteries/stream_util.hpp>
#include <batteries/suppress.hpp>
#include <batteries/type_traits.hpp>
#include <batteries/utility.hpp>

#include <boost/asio/associated_allocator.hpp>
#include <boost/asio/associated_executor.hpp>
#include <boost/asio/post.hpp>
#include <boost/intrusive/options.hpp>
#include <boost/intrusive/slist.hpp>

#include <memory>
#include <tuple>
#include <type_traits>
#include <utility>

namespace batt {

using DefaultHandlerBase = boost::intrusive::slist_base_hook<boost::intrusive::cache_last<true>,
                                                             boost::intrusive::constant_time_size<true>>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
/** A type-erased async completion handler with linked list pointers.
 */
template <typename Base, typename... Args>
class BasicAbstractHandler : public Base
{
   public:
    using Self = BasicAbstractHandler;

    /** Deleter for use in std::unique_ptr.
     */
    struct Deleter {
        void operator()(Self* handler) const
        {
            if (handler != nullptr) {
                handler->destroy();
            }
        }
    };

    /** AbstractHandler is not copy-constructible.
     */
    BasicAbstractHandler(const Self&) = delete;

    /** AbstractHandler is not copy-assignable.
     */
    Self& operator=(const Self&) = delete;

    /** Invoke the handler; notify should delete `this` as a side-effect.
     */
    virtual void notify(Args... args) = 0;

    /** Release memory associated with this handler and destroy the implementation object, without invoking
     * it.
     */
    virtual void destroy() = 0;

    /** Print the type and any other associated information about the handler.
     */
    virtual void dump(std::ostream& out) = 0;

   protected:
    BasicAbstractHandler() = default;

    // The handler should only be deleted from inside `notify`.
    //
    virtual ~BasicAbstractHandler() = default;
};

template <typename... Args>
using AbstractHandler = BasicAbstractHandler<DefaultHandlerBase, Args...>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename HandlerFn, typename Base, typename... Args>
class BasicHandlerImpl : public BasicAbstractHandler<Base, Args...>
{
   public:
    using Self = BasicHandlerImpl;

    static_assert(std::is_same_v<HandlerFn, std::decay_t<HandlerFn>>,
                  "HandlerFn may not be a reference type");

    static_assert(batt::IsCallable<HandlerFn, Args...>{}, "HandlerFn must be callable with (Args...)");

    using allocator_type = typename std::allocator_traits<
        boost::asio::associated_allocator_t<HandlerFn>>::template rebind_alloc<Self>;

    template <typename HandlerFnArg>
    static Self* make_new(HandlerFnArg&& h)
    {
        allocator_type local_allocator = std::move(boost::asio::get_associated_allocator(h));
        void* memory = local_allocator.allocate(1);
        return new (memory) Self{BATT_FORWARD(h)};
    }

    template <typename HandlerFnArg>
    static Self* make_new(HandlerFnArg&& h, usize extra_bytes)
    {
        allocator_type local_allocator = std::move(boost::asio::get_associated_allocator(h));
        void* memory = local_allocator.allocate(1 + (extra_bytes + sizeof(Self) - 1) / sizeof(Self));
        return new (memory) Self{BATT_FORWARD(h)};
    }

    template <typename HandlerFnArg, typename = batt::EnableIfNoShadow<Self, HandlerFnArg&&>>
    explicit BasicHandlerImpl(HandlerFnArg&& h) noexcept : fn_(BATT_FORWARD(h))
    {
    }

    void notify(Args... args) override
    {
        this->consume_impl([&](auto&& local_fn) {
            std::move(local_fn)(BATT_FORWARD(args)...);
        });
    }

    void destroy() override
    {
        this->consume_impl([](auto&& /*local_fn*/) { /* do nothing */ });
    }

    void dump(std::ostream& out) override
    {
        out << "HandlerImpl<" << name_of(StaticType<HandlerFn>{}) << ">{}";
    }

    HandlerFn& get_fn()
    {
        return this->fn_;
    }

   private:
    template <typename FnAction>
    void consume_impl(FnAction&& fn_action)
    {
        allocator_type local_allocator = std::move(boost::asio::get_associated_allocator(this->fn_));
        HandlerFn local_fn = std::move(this->fn_);
        this->~Self();
        local_allocator.deallocate(this, 1);

        BATT_FORWARD(fn_action)(BATT_FORWARD(local_fn));
    }

    HandlerFn fn_;
};

template <typename HandlerFn, typename... Args>
using HandlerImpl = BasicHandlerImpl<HandlerFn, DefaultHandlerBase, Args...>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename... Args>
class UniqueHandler
{
   public:
    template <typename Fn, typename = EnableIfNoShadow<UniqueHandler, Fn>,
              typename = std::enable_if_t<IsCallable<std::decay_t<Fn>, Args...>::value>>
    explicit UniqueHandler(Fn&& fn) noexcept
        : handler_{HandlerImpl<std::decay_t<Fn>, Args...>::make_new(BATT_FORWARD(fn))}
    {
    }

    UniqueHandler() = default;

    UniqueHandler(const UniqueHandler&) = delete;
    UniqueHandler& operator=(const UniqueHandler&) = delete;

    UniqueHandler(UniqueHandler&&) = default;
    UniqueHandler& operator=(UniqueHandler&&) = default;

    ~UniqueHandler() noexcept
    {
    }

    explicit operator bool() const
    {
        return this->handler_ != nullptr;
    }

    void operator()(Args... args)
    {
        if (this->handler_ != nullptr) {
            auto* local_handler = this->handler_.release();
            local_handler->notify(BATT_FORWARD(args)...);
        }
    }

    friend inline std::ostream& operator<<(std::ostream& out, const UniqueHandler& t)
    {
        if (t.handler_ == nullptr) {
            return out << "<nullptr>";
        }
        out << (void*)t.handler_.get() << ": ";
        t.handler_->dump(out);
        return out;
    }

   private:
    std::unique_ptr<AbstractHandler<Args...>, typename AbstractHandler<Args...>::Deleter> handler_;
};

BATT_STATIC_ASSERT_EQ(sizeof(UniqueHandler<>), sizeof(void*));

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename Base, typename... Args>
using BasicHandlerList = boost::intrusive::slist<BasicAbstractHandler<Base, Args...>,  //
                                                 boost::intrusive::cache_last<true>,   //
                                                 boost::intrusive::constant_time_size<true>>;

template <typename... Args>
using HandlerList = BasicHandlerList<DefaultHandlerBase, Args...>;

template <typename Base, typename... Args>
inline void push_handler_prealloc(boost::intrusive::slist<BasicAbstractHandler<Base, Args...>,  //
                                                          boost::intrusive::cache_last<true>,   //
                                                          boost::intrusive::constant_time_size<true>>* list,
                                  BasicAbstractHandler<Base, Args...>* handler)
{
    list->push_back(*handler);
}

template <typename... Args, typename Base, typename HandlerFn>
inline void push_handler(boost::intrusive::slist<BasicAbstractHandler<Base, Args...>,  //
                                                 boost::intrusive::cache_last<true>,   //
                                                 boost::intrusive::constant_time_size<true>>* list,
                         HandlerFn&& fn)
{
    push_handler_prealloc(list, BasicHandlerImpl<HandlerFn, Base, Args...>::make_new(BATT_FORWARD(fn)));
}

template <typename... Params, typename... Args, typename Base>
inline void invoke_all_handlers(boost::intrusive::slist<BasicAbstractHandler<Base, Params...>,  //
                                                        boost::intrusive::cache_last<true>,     //
                                                        boost::intrusive::constant_time_size<true>>* handlers,
                                Args&&... args)
{
    while (!handlers->empty()) {
        BasicAbstractHandler<Base, Params...>& l = handlers->front();
        handlers->pop_front();
        l.notify(args...);
    }
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
//
template <typename InnerFn, typename OuterFn>
class HandlerBinder
{
   public:
    using allocator_type = boost::asio::associated_allocator_t<InnerFn>;

    template <typename InnerFnArg, typename OuterFnArg>
    explicit HandlerBinder(InnerFnArg&& inner, OuterFnArg&& outer)
        : inner_fn_{BATT_FORWARD(inner)}
        , outer_fn_{BATT_FORWARD(outer)}
    {
    }

    allocator_type get_allocator() const noexcept
    {
        return boost::asio::get_associated_allocator(this->inner_fn_);
    }

    template <typename... Args,
              typename = std::enable_if_t<batt::IsCallable<OuterFn&&, InnerFn&&, Args&&...>{}>>
    void operator()(Args&&... args) noexcept(noexcept(std::declval<HandlerBinder*>()->outer_fn_(
        BATT_FORWARD(std::declval<HandlerBinder*>()->inner_fn_), std::declval<Args>()...)))
    {
        this->outer_fn_(BATT_FORWARD(this->inner_fn_), BATT_FORWARD(args)...);
    }

    InnerFn inner_fn_;
    OuterFn outer_fn_;
};

template <typename InnerFn, typename OuterFn>
HandlerBinder<std::decay_t<InnerFn>, std::decay_t<OuterFn>> bind_handler(InnerFn&& inner, OuterFn&& outer)
{
    return HandlerBinder<std::decay_t<InnerFn>, std::decay_t<OuterFn>>{BATT_FORWARD(inner),
                                                                       BATT_FORWARD(outer)};
}

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++

BATT_SUPPRESS_IF_GCC("-Wfree-nonheap-object")

/** Abstract base for HandlerMemory<kSize>.  Decouples users of HandlerMemory from knowledge of the static
 * memory size.
 */
class HandlerMemoryBase
{
   public:
    explicit HandlerMemoryBase(const MutableBuffer& buffer) noexcept : buffer_{buffer}
    {
    }

    explicit HandlerMemoryBase(void* ptr, usize size) noexcept : buffer_{ptr, size}
    {
    }

    HandlerMemoryBase(const HandlerMemoryBase&) = delete;
    HandlerMemoryBase& operator=(const HandlerMemoryBase&) = delete;

    virtual ~HandlerMemoryBase() = default;

    void* allocate(usize size)
    {
        if (!this->in_use_ && size <= this->buffer_.size()) {
            this->in_use_ = true;
            return this->buffer_.data();
        } else {
            return ::operator new(size);
        }
    }

    void deallocate(void* pointer) noexcept
    {
        if (pointer == this->buffer_.data()) {
            this->in_use_ = false;
        } else {
            ::operator delete(pointer);
        }
    }

    bool in_use() const noexcept
    {
        return this->in_use_;
    }

   private:
    // The handler memory.
    //
    MutableBuffer buffer_;

    // Tracks whether the memory is currently in use.
    //
    bool in_use_ = false;
};

/** A chunk of memory that can be attached to an async completion handler.
 */
template <usize kSize>
class HandlerMemory : public HandlerMemoryBase
{
   public:
    HandlerMemory() noexcept : HandlerMemoryBase{&this->storage_, sizeof(this->storage_)}
    {
    }

    HandlerMemory(const HandlerMemory&) = delete;
    HandlerMemory& operator=(const HandlerMemory&) = delete;

   private:
    // The memory.
    //
    std::aligned_storage_t<kSize> storage_;
};

template <typename T>
class HandlerAllocator;

template <typename T, typename U>
bool operator==(const HandlerAllocator<T>& left, const HandlerAllocator<U>& right) noexcept;

template <typename T, typename U>
bool operator!=(const HandlerAllocator<T>& left, const HandlerAllocator<U>& right) noexcept;

/** An allocator associated with a completion handler.
 *
 * Designed to satisfy the C++11 minimal allocator requirements.
 */
template <typename T>
class HandlerAllocator
{
   public:
    template <typename T_, typename U>
    friend bool operator==(const HandlerAllocator<T_>& left, const HandlerAllocator<U>& right) noexcept;

    template <typename T_, typename U>
    friend bool operator!=(const HandlerAllocator<T_>& left, const HandlerAllocator<U>& right) noexcept;

    using value_type = T;

    explicit HandlerAllocator(HandlerMemoryBase& mem) : memory_(mem)
    {
    }

    template <typename U>
    HandlerAllocator(const HandlerAllocator<U>& that) noexcept : memory_(that.memory_)
    {
    }

    T* allocate(usize n) const
    {
        return static_cast<T*>(this->memory_.allocate(sizeof(T) * n));
    }

    void deallocate(T* p, usize /*n*/) const
    {
        return this->memory_.deallocate(p);
    }

   private:
    template <typename>
    friend class HandlerAllocator;

    // The attached memory.
    //
    HandlerMemoryBase& memory_;
};

template <typename T, typename U>
bool operator==(const HandlerAllocator<T>& left, const HandlerAllocator<U>& right) noexcept
{
    return &left.memory_ == &right.memory_;
}

template <typename T, typename U>
bool operator!=(const HandlerAllocator<T>& left, const HandlerAllocator<U>& right) noexcept
{
    return !(left == right);
}

/** Wrapper for an async completion handler type `Handler`.  Provides an associated allocator that allocates
 * from a `HandlerMemory` instance.
 */
template <typename Handler>
class CustomAllocHandler
{
   public:
    using allocator_type = HandlerAllocator<Handler>;

    template <typename HandlerArg>
    CustomAllocHandler(HandlerMemoryBase& m, HandlerArg&& h)
        : memory_{&m}
        , handler_(std::forward<HandlerArg>(h))
    {
    }

    allocator_type get_allocator() const noexcept
    {
        return allocator_type{*this->memory_};
    }

    template <typename... Args>
    void operator()(Args&&... args) noexcept(noexcept(std::declval<Handler&>()(std::declval<Args>()...)))
    {
        handler_(std::forward<Args>(args)...);
    }

    const Handler& get_handler() const noexcept
    {
        return this->handler_;
    }

   private:
    // The attached memory.
    //
    HandlerMemoryBase* memory_;

    // The wrapped completion handler.
    //
    Handler handler_;
};

/** Helper function to wrap a handler object to add custom allocation.
 */
template <typename Handler>
inline CustomAllocHandler<std::decay_t<Handler>> make_custom_alloc_handler(HandlerMemoryBase& m, Handler&& h)
{
    return CustomAllocHandler<std::decay_t<Handler>>{m, std::forward<Handler>(h)};
}

/** \brief Posts a handler using its associated executor, binding its arguments.
 */
template <typename Handler, typename... Args>
void post_handler(Handler&& handler, Args&&... args) noexcept
{
    auto executor = boost::asio::get_associated_executor(handler);
    boost::asio::post(                                                                 //
        executor,                                                                      //
        bind_handler(                                                                  //
            BATT_FORWARD(handler),                                                     //
            [args = std::make_tuple(BATT_FORWARD(args)...)](auto&& handler) mutable {  //
                std::apply(BATT_FORWARD(handler), std::move(args));
            }));
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_HANDLER_HPP
