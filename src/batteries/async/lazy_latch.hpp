//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/async/lazy_latch_decl.hpp>

#include <batteries/config.hpp>

#include <batteries/async/watch.ipp>

#include <batteries/async/lazy_latch_impl.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/lazy_latch_impl.hpp>
#endif
