// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_TYPES_HPP
#define BATTERIES_ASYNC_TYPES_HPP

#include <ostream>

namespace batt {

enum struct WaitForResource : bool {
    kFalse = false,
    kTrue = true,
};

inline std::ostream& operator<<(std::ostream& out, WaitForResource t)
{
    switch (t) {
    case WaitForResource::kFalse:
        return out << "WaitForResource::kFalse";

    case WaitForResource::kTrue:  // fall-through
    default:
        return out << "WaitForResource::kTrue";
    }
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_TYPES_HPP
