//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_DUMP_TASKS_HPP
#define BATTERIES_ASYNC_DUMP_TASKS_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/task.hpp>

#include <batteries/assert.hpp>
#include <batteries/optional.hpp>
#include <batteries/suppress.hpp>

#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/io_context.hpp>

BATT_SUPPRESS_IF_GCC("-Wsuggest-override")
BATT_SUPPRESS_IF_CLANG("-Wsuggest-override")
//
#include <boost/asio/signal_set.hpp>
#include <boost/exception/diagnostic_information.hpp>
//
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>
#include <type_traits>

namespace batt {

namespace detail {

class SigInfoHandler
{
   public:
    static constexpr int kSignalNum =
#if defined(BATT_PLATFORM_IS_LINUX)
        SIGUSR1
#elif defined(BATT_PLATFORM_IS_APPLE)
        SIGINFO
#else
        0
#endif
        ;

    using WorkGuard =
        batt::Optional<boost::asio::executor_work_guard<boost::asio::io_context::executor_type>>;

    static SigInfoHandler& instance()
    {
        static std::aligned_storage_t<sizeof(SigInfoHandler)> storage_;
        static SigInfoHandler* instance_ = [] {
            try {
                return new (&storage_) SigInfoHandler;
            } catch (...) {
                BATT_PANIC() << boost::current_exception_diagnostic_information();
                BATT_UNREACHABLE();
            }
        }();
        return *instance_;
    }

    SigInfoHandler()
    {
        this->sig_info_thread_.detach();
    }

    void start()
    {
        BATT_CHECK(this->sig_info_);

#ifdef BATT_PLATFORM_IS_WINDOWS
        BATT_LOG_WARNING() << "Signal handling not yet supported on Windows";
#else   // BATT_PLATFORM_IS_WINDOWS
        this->wait_for_signal();
#endif  // BATT_PLATFORM_IS_WINDOWS
    }

    void halt()
    {
        const bool halted_prior = this->halted_.exchange(true);
        if (halted_prior) {
            return;
        }

#ifndef BATT_PLATFORM_IS_WINDOWS
        {
            batt::ErrorCode ec;
            this->sig_info_->cancel(ec);
        }
#endif  // BATT_PLATFORM_IS_WINDOWS
        this->sig_info_work_guard_ = None;
        this->sig_info_io_.stop();
        BATT_LOG_INFO() << "signal handlers cancelled";
    }

    void join()
    {
        if (this->sig_info_thread_.joinable()) {
            this->sig_info_thread_.join();
        }
    }

    void wait_for_signal()
    {
        this->last_sig_info_ = std::chrono::steady_clock::now();
        this->sig_info_->async_wait([this](auto&&... args) {
            return this->handle_signal(BATT_FORWARD(args)...);
        });
    }

    void handle_signal(const boost::system::error_code& ec, int signal_n)
    {
        if (ec || this->halted_.load()) {
            return;
        }

        using namespace std::literals::chrono_literals;

        const bool force = (std::chrono::steady_clock::now() - this->last_sig_info_) <= 5s;
        std::cerr << "[batt::SigInfoHandler::operator()(" << signal_n << "); force=" << force << "]"
                  << std::endl;

        batt::Task::backtrace_all(force);
        this->last_sig_info_ = std::chrono::steady_clock::now();
        this->wait_for_signal();
    }

   private:
    std::atomic<bool> halted_{false};

    boost::asio::io_context sig_info_io_;

    Optional<WorkGuard> sig_info_work_guard_{this->sig_info_io_.get_executor()};

    Optional<boost::asio::signal_set> sig_info_{this->sig_info_io_, SigInfoHandler::kSignalNum};

    std::chrono::steady_clock::time_point last_sig_info_ = std::chrono::steady_clock::now();

    std::thread sig_info_thread_{[this] {
        this->sig_info_io_.run();
    }};
};

}  // namespace detail

inline bool enable_dump_tasks()
{
    static bool initialized_ = [] {
#ifdef BATT_PLATFORM_IS_WINDOWS
        BATT_LOG_WARNING() << "Signal handling not yet supported on Windows";
#else   // BATT_PLATFORM_IS_WINDOWS
        detail::SigInfoHandler::instance().start();
#endif  // BATT_PLATFORM_IS_WINDOWS
        return true;
    }();

    return initialized_;
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_DUMP_TASKS_HPP
