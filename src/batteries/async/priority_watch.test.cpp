//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/async/priority_watch.hpp>
//
#include <batteries/async/priority_watch.hpp>

#include <batteries/seq.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace {

TEST(AsyncPriorityWatchTest, HeapInsert)
{
    using namespace batt::int_types;
    using batt::detail::PriorityHeap;
    using batt::detail::PriorityHeapItem;

    constexpr usize kItemCount = 10;

    std::array<std::aligned_storage_t<sizeof(PriorityHeapItem<int>)>, kItemCount> storage;
    std::array<PriorityHeapItem<int>*, storage.size()> p_items;

    for (int value = 0; value < (int)storage.size(); ++value) {
        p_items[value] = new (&storage[value]) PriorityHeapItem<int>{value};
    }

    while (std::next_permutation(p_items.begin(), p_items.end(), [](auto* p_first, auto* p_second) {
        return p_first->order() < p_second->order();
    })) {
        PriorityHeap<int> heap;
        int min_value = std::numeric_limits<int>::max();

        EXPECT_TRUE(heap.empty());

        usize expect_size = 0;
        for (auto* p_item : p_items) {
            EXPECT_EQ(heap.size(), expect_size);
            EXPECT_FALSE(p_item->is_linked());

            heap.insert(p_item);
            expect_size += 1;

            EXPECT_EQ(heap.size(), expect_size);

            min_value = std::min(min_value, p_item->order());

            EXPECT_EQ(min_value, heap.min_value());
            EXPECT_FALSE(heap.empty());
        }

        PriorityHeap<int>::StackVec stack;

        EXPECT_TRUE(heap.check_invariants(&stack));
        EXPECT_EQ(min_value, 0);

        while (!heap.empty()) {
            EXPECT_EQ(expect_size, heap.size());
            EXPECT_EQ(min_value, heap.min_value());

            auto* item = heap.remove();

            ASSERT_NE(item, nullptr);
            EXPECT_EQ(item->order(), min_value);
            EXPECT_FALSE(item->is_linked());

            expect_size -= 1;

            EXPECT_EQ(expect_size, heap.size());

            min_value += 1;
        }
        EXPECT_TRUE(heap.empty());
        EXPECT_EQ(min_value, int(kItemCount));
    }
}

}  // namespace
