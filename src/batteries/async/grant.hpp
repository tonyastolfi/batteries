//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_GRANT_HPP
#define BATTERIES_ASYNC_GRANT_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/scalable_grant.hpp>

namespace batt {

using Grant = ScalableGrant;

}  //namespace batt

#endif  // BATTERIES_ASYNC_GRANT_HPP
