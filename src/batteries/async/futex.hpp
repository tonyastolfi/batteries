#pragma once
#ifndef BATTERIES_ASYNC_FUTEX_HPP
#define BATTERIES_ASYNC_FUTEX_HPP

#include <batteries/config.hpp>
//

#include <batteries/int_types.hpp>
#include <batteries/syscall_retry.hpp>

#ifdef BATT_PLATFORM_IS_LINUX
#include <linux/futex.h>
#include <sched.h>
#include <sys/syscall.h>
#endif

#include <atomic>
#include <thread>
#include <type_traits>

namespace batt {

#if defined(BATT_PLATFORM_IS_LINUX)

static_assert(std::is_same_v<int, i32>);

inline int futex_impl(u32* uaddr, i32 futex_op, u32 val, const struct timespec* timeout, u32* uaddr2,
                      u32 val3)
{
    return syscall_retry([&] {
        return syscall(SYS_futex, uaddr, futex_op, val, timeout, uaddr2, val3);
    });
}

inline int futex_wait(std::atomic<u32>* var, u32 last_seen)
{
    static_assert(sizeof(std::atomic<u32>) == sizeof(u32));
    return futex_impl((u32*)var, FUTEX_WAIT_PRIVATE, last_seen, NULL, NULL, 0);
}

inline int futex_notify(std::atomic<u32>* var, u32 count = 1)
{
    static_assert(sizeof(std::atomic<u32>) == sizeof(u32));
    return futex_impl((u32*)var, FUTEX_WAKE_PRIVATE, count, NULL, NULL, 0);
}

inline void spin_yield()
{
    sched_yield();
}

#else

inline int futex_wait(std::atomic<u32>*, u32)
{
    return 0;
}

inline int futex_notify(std::atomic<u32>*, u32 = 1)
{
    return 0;
}

inline void spin_yield()
{
    std::this_thread::yield();
}

#endif

}  //namespace batt

#endif  // BATTERIES_ASYNC_FUTEX_HPP
