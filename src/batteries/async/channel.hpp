//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_CHANNEL_HPP
#define BATTERIES_ASYNC_CHANNEL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/watch.hpp>

#include <batteries/optional.hpp>
#include <batteries/status.hpp>

#include <type_traits>

namespace batt {

/** A Channel<T> is a one-way, unbuffered, SPSC (single-producer, single consumer)
 * communication/synchronization primitive.
 */
template <typename T>
class Channel
{
   public:
    template <typename Handler,
              typename = std::enable_if_t<std::is_invocable_v<Handler&&, const Status&, T&>>>
    static void invoke_write_handler(Handler&& handler, const batt::Status& status,
                                     T& value) noexcept(noexcept(BATT_FORWARD(handler)(status, value)))
    {
        BATT_FORWARD(handler)(status, value);
    }

    template <typename Handler,
              typename = std::enable_if_t<!std::is_invocable_v<Handler&&, const Status&, T&> &&
                                          std::is_invocable_v<Handler&&, const Status&>>,
              typename = void>
    static void invoke_write_handler(Handler&& handler, const batt::Status& status,
                                     T& /*value*/) noexcept(noexcept(BATT_FORWARD(handler)(status)))
    {
        BATT_FORWARD(handler)(status);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Construct a new Channel object.
     */
    Channel()
    {
    }

    /** Channel is not copy-constructible.
     */
    Channel(const Channel&) = delete;

    /** Channel is not copy-assignable.
     */
    Channel& operator=(const Channel&) = delete;

    /** Destroy the Channel; calls this->close_for_write() and waits for any active reader to finish (via
     *  Channel::consume).
     */
    ~Channel()
    {
        this->close_for_write();
        this->await_inactive();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Returns true iff there is currently an object available on the channel.
     */
    bool is_active() const;

    /** \brief Blocks the current Task until the Channel is inactive.
     */
    void await_inactive() noexcept
    {
        this->read_count_.await_equal(this->write_count_.get_value()).IgnoreError();
    }

    template <typename Handler>
    void async_wait_inactive(Handler&& handler) noexcept
    {
        const i32 observed_read_count = this->read_count_.get_value();

        this->async_wait_inactive_impl(observed_read_count, BATT_FORWARD(handler));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Start a new read and invoke the passed handler when the next value is written to the channel.
     *
     * Will invoke the handler immediately with batt::StatusCode::kClosed if either:
     *     - The channel was closed for read prior to calling async_read
     *     - The channel is closed for write, before or after the call to async_read
     */
    template <typename Handler = void(StatusOr<T&>)>
    void async_read(Handler&& handler);

    /** Wait for an object to be written to the Channel, then return a reference to that object.  The referred
     *  object will stay valid until Channel::consume is called.
     */
    StatusOr<T&> read();

    /** Release the last value read on the Channel.  Panic if consume() is called without a prior successful
     *  call to read() (indicating that this->is_active() is true).
     */
    void consume();

    /** Unblock all ongoing calls to Channel::write / Channel::async_write and cause all future calls to fail
     * with closed error status.
     */
    void close_for_read();

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** Write a value to the Channel.  Panic if there is currently an ongoing write (single-producer).
     *
     * The passed handler is invoked once the value is consumed via this->read() / this->consume().
     */
    template <typename Handler = void(Status, T&)>
    void async_write(T& value, Handler&& handler);

    /** Blocking (Task::await) version of Channel::async_write.  This function will not return until the
     * passed value has been consumed.  The value is guaranteed not to be copied; the consumer will see the
     * same exact object passed into write.
     */
    Status write(T& value);

    /** Unblock all ongoing calls to read() and cause all future calls to fail with closed error status.
     */
    void close_for_write();

   private:
    //+++++++++++-+-+--+----- --- -- -  -  -   -

    template <typename Handler>
    void async_wait_inactive_impl(i32 observed_read_count, Handler&& handler) noexcept
    {
        if (observed_read_count == this->write_count_.get_value()) {
            BATT_FORWARD(handler)();
        } else {
            this->read_count_.async_wait(
                observed_read_count,
                bind_handler(BATT_FORWARD(handler), [this](auto&& handler, StatusOr<i32> new_read_count) {
                    this->async_wait_inactive_impl(*new_read_count, BATT_FORWARD(handler));
                }));
        }
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    Watch<i32> read_count_{0};
    Watch<i32> write_count_{0};
    T* value_ = nullptr;
};

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Handler>
void Channel<T>::async_read(Handler&& handler)
{
    if (this->read_count_.is_closed()) {
        handler(StatusOr<T&>{StatusCode::kClosed});
        return;
    }

    const i32 observed_write_count = this->write_count_.get_value();
    const i32 observed_read_count = this->read_count_.get_value();

    if (observed_write_count > observed_read_count) {
        handler(StatusOr<T&>{*this->value_});
        return;
    }

    this->write_count_.async_wait(
        observed_write_count,
        bind_handler(BATT_FORWARD(handler),
                     [this, observed_read_count](Handler&& handler, StatusOr<i32> updated_write_count) {
                         if (!updated_write_count.ok()) {
                             handler(StatusOr<T&>{updated_write_count.status()});
                             return;
                         }
                         BATT_CHECK_GT(*updated_write_count, observed_read_count);
                         handler(StatusOr<T&>{*this->value_});
                     }));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Handler>
void Channel<T>::async_write(T& value, Handler&& handler)
{
    BATT_CHECK(!this->is_active());

    if (this->write_count_.is_closed()) {
        Channel::invoke_write_handler(BATT_FORWARD(handler), Status{StatusCode::kClosed}, value);
        return;
    }

    this->value_ = &value;

    const i32 last_seen = this->read_count_.get_value();
    const i32 target = this->write_count_.fetch_add(1) + 1;

    BATT_CHECK_EQ(last_seen + 1, target);

    this->read_count_.async_wait(
        last_seen,
        bind_handler(BATT_FORWARD(handler), [this, target](auto&& handler, StatusOr<i32> observed) {
            if (observed.ok()) {
                BATT_CHECK_EQ(target, *observed);
                BATT_CHECK_NOT_NULLPTR(this->value_);
            }

            T* const consumed_value = this->value_;
            this->value_ = nullptr;
            Channel::invoke_write_handler(BATT_FORWARD(handler), observed.status(), *consumed_value);
        }));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline bool Channel<T>::is_active() const
{
    return (this->write_count_.get_value() - this->read_count_.get_value()) > 0;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline StatusOr<T&> Channel<T>::read()
{
    BATT_DEBUG_INFO("Channel::read()" << BATT_INSPECT(this->write_count_.get_value())
                                      << BATT_INSPECT(this->read_count_.get_value()));

    StatusOr<i64> result = this->write_count_.await_not_equal(this->read_count_.get_value());
    BATT_REQUIRE_OK(result);

    return *this->value_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Channel<T>::consume()
{
    BATT_CHECK(this->is_active());
    this->read_count_.fetch_add(1);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline Status Channel<T>::write(T& value)
{
    BATT_CHECK(!this->is_active());

    if (this->write_count_.is_closed()) {
        return {StatusCode::kClosed};
    }

    this->value_ = &value;
    auto on_scope_exit = batt::finally([&] {
        this->value_ = nullptr;
    });
    const i32 target = this->write_count_.fetch_add(1) + 1;

    BATT_DEBUG_INFO("Channel::write()" << BATT_INSPECT(this->write_count_.get_value())
                                       << BATT_INSPECT(this->read_count_.get_value())
                                       << BATT_INSPECT(target));

    Status consumed = this->read_count_.await_equal(target);
    BATT_REQUIRE_OK(consumed);

    return OkStatus();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Channel<T>::close_for_read()
{
    this->read_count_.close();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline void Channel<T>::close_for_write()
{
    this->write_count_.close();
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_CHANNEL_HPP
