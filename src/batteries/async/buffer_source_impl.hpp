//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_BUFFER_SOURCE_IMPL_HPP
#define BATTERIES_ASYNC_BUFFER_SOURCE_IMPL_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/buffer_source.hpp>
#include <batteries/utility.hpp>

namespace batt {

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL BufferSource::operator bool() const
{
    return bool{this->impl_};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL usize BufferSource::size() const
{
    return this->impl_->size();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL StatusOr<SmallVec<ConstBuffer, 2>> BufferSource::fetch_at_least(i64 min_count)
{
    return this->impl_->fetch_at_least(min_count);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void BufferSource::consume(i64 count)
{
    return this->impl_->consume(count);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void BufferSource::close_for_read()
{
    return this->impl_->close_for_read();
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_BUFFER_SOURCE_IMPL_HPP
