//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/scalable_grant.hpp>
//
#include <batteries/async/scalable_grant_impl.hpp>

#endif  // !BATT_HEADER_ONLY
