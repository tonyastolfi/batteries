//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_LAZY_LATCH_IMPL_IPP
#define BATTERIES_ASYNC_LAZY_LATCH_IMPL_IPP

#include <batteries/config.hpp>

#if BATT_HEADER_ONLY
#include <batteries/async/watch_impl.hpp>
#endif

#include <boost/exception/diagnostic_information.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
/*explicit*/ LazyLatch<T, InitFn>::LazyLatch(InitFn&& init_fn) noexcept
{
    new (std::addressof(this->storage_)) InitFn{std::move(init_fn)};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
LazyLatch<T, InitFn>::~LazyLatch() noexcept
{
    const u32 observed_state = this->state_.get_value();

    switch (observed_state) {
    case State::kInitial:
        this->stored_init_fn().~InitFn();
        break;

    case State::kReady:
        this->stored_result().~StatusOr<T>();
        break;

    default:
        BATT_PANIC() << "Invalid LazyLatch state: " << observed_state;
        BATT_UNREACHABLE();
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline void LazyLatch<T, InitFn>::trigger()
{
    const u32 prior_state = this->state_.fetch_or(kTriggered);
    if (prior_state != kInitial) {
        return;
    }

    this->stored_init_fn()(this);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline bool LazyLatch<T, InitFn>::is_ready() const
{
    return this->state_.get_value() == kReady;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline StatusOr<T> LazyLatch<T, InitFn>::await() & noexcept
{
    this->trigger();

    Status status = this->state_.await_equal(kReady);
    BATT_REQUIRE_OK(status);

    return this->get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline StatusOr<T> LazyLatch<T, InitFn>::await() && noexcept
{
    this->trigger();

    Status status = this->state_.await_equal(kReady);
    BATT_REQUIRE_OK(status);

    return std::move(*this).get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline StatusOr<T> LazyLatch<T, InitFn>::poll()
{
    if (this->state_.get_value() != kReady) {
        return Status{StatusCode::kUnavailable};
    }
    return this->get_ready_value_or_panic();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline StatusOr<T> LazyLatch<T, InitFn>::get_ready_value_or_panic() & noexcept
{
    return this->stored_result();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
inline StatusOr<T> LazyLatch<T, InitFn>::get_ready_value_or_panic() && noexcept
{
    return std::move(this->stored_result());
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
template <typename Handler>
inline void LazyLatch<T, InitFn>::async_get(Handler&& handler)
{
    this->trigger();
    this->state_.async_wait(kTriggered, bind_handler(BATT_FORWARD(handler), AsyncGetHandler{this}));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
void LazyLatch<T, InitFn>::notify(StatusOr<T>&& value) /*override*/
{
    try {
        // Destruct the init function so we can reuse the storage for the result.
        //
        this->stored_init_fn().~InitFn();

        // Place the result in this->storage_.
        //
        new (std::addressof(this->storage_)) StatusOr<T>{std::move(value)};

    } catch (...) {
        // If we throw for any reason, panic; `InitFn` must be no-throw destructible and type `T` must be
        // no-throw move constructible.
        //
        BATT_PANIC() << "Uncaught exception: " << boost::current_exception_diagnostic_information();
        BATT_UNREACHABLE();
    }

    BATT_CHECK_EQ(this->state_.set_value(kReady), kTriggered);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
void LazyLatch<T, InitFn>::destroy() /*override*/
{
    this->notify(Status{StatusCode::kCancelled});
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
void LazyLatch<T, InitFn>::dump(std::ostream& out) /*override*/
{
    out << "LazyLatch{}";
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
InitFn& LazyLatch<T, InitFn>::stored_init_fn() noexcept
{
    BATT_CHECK_NE(this->state_.get_value(), State::kReady);
    return *reinterpret_cast<InitFn*>(std::addressof(this->storage_));
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename InitFn>
StatusOr<T>& LazyLatch<T, InitFn>::stored_result() noexcept
{
    BATT_CHECK_EQ(this->state_.get_value(), State::kReady);
    return *reinterpret_cast<StatusOr<T>*>(std::addressof(this->storage_));
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_LAZY_LATCH_IMPL_IPP
