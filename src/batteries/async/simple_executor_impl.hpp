//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SIMPLE_EXECUTOR_IMPL_HPP
#define BATTERIES_ASYNC_SIMPLE_EXECUTOR_IMPL_HPP

#include <batteries/config.hpp>

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ auto SimpleExecutionContext::inside_run_on_this_thread() noexcept
    -> SimpleExecutionContext**
{
    thread_local SimpleExecutionContext* context = nullptr;
    return &context;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::get_executor() noexcept -> executor_type
{
    return executor_type{this};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::stop() noexcept -> void
{
    this->stop_.store(true);
    this->cond_.notify_all();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::is_stopped() const noexcept -> bool
{
    return this->stop_.load();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::run() noexcept -> usize
{
    usize handler_count = 0;

    Self* prev_context = this;
    std::swap(*Self::inside_run_on_this_thread(), prev_context);
    auto on_scope_exit = batt::finally([&] {
        std::swap(*Self::inside_run_on_this_thread(), prev_context);
    });

    while (!this->stop_.load() && this->work_count_.get_value() > 0) {
        bool have_more = false;

        auto next = [this, &have_more]() -> AbstractHandler<>* {
            std::unique_lock<std::mutex> lock{this->mutex_};

            while (this->queue_.empty() && !this->stop_.load() && this->work_count_.get_value() > 0) {
                this->cond_.wait(lock);
            }

            if (this->queue_.empty()) {
                return nullptr;
            }

            auto* ptr = &(this->queue_.front());
            this->queue_.pop_front();
            have_more = !this->queue_.empty();

            return ptr;
        }();

        if (have_more) {
            this->cond_.notify_one();
        } else if (this->stop_.load() || this->work_count_.get_value() == 0) {
            this->cond_.notify_all();
        }

        if (next) {
            try {
                auto on_handler_return = batt::finally([this, &handler_count] {
                    ++handler_count;
                    this->on_work_finished();
                });
                next->notify();
            } catch (...) {
                continue;
            }
        }
    }

    return handler_count;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::reset() noexcept -> void
{
    this->stop_.store(false);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::get_allocator() const -> std::allocator<void>
{
    return this->allocator_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::work_count() noexcept -> Watch<i64>&
{
    return this->work_count_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::wake() -> void
{
    this->cond_.notify_all();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::on_work_started() noexcept -> void
{
    const i64 prior_count = this->work_count_.fetch_add(1);
    BATT_CHECK_NE(prior_count + 1u, 0u);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL auto SimpleExecutionContext::on_work_finished() noexcept -> void
{
    const i64 prior_count = this->work_count_.fetch_sub(1);
    BATT_CHECK_GT(prior_count, 0u);
    if (prior_count == 1) {
        {
            std::unique_lock<std::mutex> lock{this->mutex_};
            if (this->work_count_.get_value() != 0) {
                return;
            }
        }
        this->cond_.notify_all();
    }
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------
// Explicit instantiations.
//
#if 0
    template class BasicExecutor<boost::asio::execution::outstanding_work_t::tracked_t,
                             boost::asio::execution::blocking_t::never_t,
                             boost::asio::execution::relationship_t::fork_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::untracked_t,
                             boost::asio::execution::blocking_t::never_t,
                             boost::asio::execution::relationship_t::fork_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::tracked_t,
                             boost::asio::execution::blocking_t::possibly_t,
                             boost::asio::execution::relationship_t::fork_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::untracked_t,
                             boost::asio::execution::blocking_t::possibly_t,
                             boost::asio::execution::relationship_t::fork_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::tracked_t,
                             boost::asio::execution::blocking_t::never_t,
                             boost::asio::execution::relationship_t::continuation_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::untracked_t,
                             boost::asio::execution::blocking_t::never_t,
                             boost::asio::execution::relationship_t::continuation_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::tracked_t,
                             boost::asio::execution::blocking_t::possibly_t,
                             boost::asio::execution::relationship_t::continuation_t>;

template class BasicExecutor<boost::asio::execution::outstanding_work_t::untracked_t,
                             boost::asio::execution::blocking_t::possibly_t,
                             boost::asio::execution::relationship_t::continuation_t>;

//#=##=##=#==#=#==#===#+==#+==========+==+=+=+=+=+=++=+++=+++++=-++++=-+++++++++++
// Type requirement checks.

static_assert(std::is_constructible<boost::asio::any_io_executor, SimpleExecutor>{},
              "If this check fails, hopefully one or more of the following more scoped checks will fail as "
              "well, which will help with debugging!");

static_assert(
    boost::asio::execution::can_execute<SimpleExecutor, boost::asio::execution::invocable_archetype>::value,
    "");

static_assert(std::is_same_v<bool, decltype(std::declval<const SimpleExecutor>() ==
                                            std::declval<const SimpleExecutor>())>,
              "");

static_assert(std::is_same_v<bool, decltype(std::declval<const SimpleExecutor>() !=
                                            std::declval<const SimpleExecutor>())>,
              "");

static_assert(std::is_nothrow_copy_constructible<SimpleExecutor>::value, "");
static_assert(std::is_nothrow_destructible<SimpleExecutor>::value, "");
static_assert(boost::asio::traits::equality_comparable<SimpleExecutor>::is_valid, "");
static_assert(boost::asio::traits::equality_comparable<SimpleExecutor>::is_noexcept, "");

static_assert(boost::asio::execution::is_executor_v<SimpleExecutor>, "");

static_assert(boost::asio::execution::context_as_t<boost::asio::execution_context&>::is_applicable_property_v<
                  SimpleExecutor>,
              "");

static_assert(
    boost::asio::can_query<SimpleExecutor,
                           boost::asio::execution::context_as_t<boost::asio::execution_context&>>::value ||
        true,
    "");

static_assert(boost::asio::is_applicable_property_v<
                  SimpleExecutor, boost::asio::execution::context_as_t<boost::asio::execution_context&>>,
              "");

static_assert(boost::asio::execution::detail::supportable_properties<
                  0, void(boost::asio::execution::context_as_t<boost::asio::execution_context&>)>::
                      template is_valid_target<SimpleExecutor>::value ||
                  true,
              "");

static_assert(
    boost::asio::execution::detail::is_valid_target_executor<
        SimpleExecutor, void(boost::asio::execution::context_as_t<boost::asio::execution_context&>)>::value,
    "");

static_assert(std::is_same_v<decltype(boost::asio::query(
                                 std::declval<const SimpleExecutor>(),
                                 boost::asio::execution::context_as<boost::asio::execution_context&>)),
                             boost::asio::execution_context&>,
              "");

static_assert(boost::asio::is_executor<SimpleExecutionContext::executor_type>::value);

static_assert(boost::asio::execution::detail::is_executor_of_impl<
              SimpleExecutionContext::executor_type, boost::asio::execution::invocable_archetype>::value);

static_assert(boost::asio::execution::is_executor<SimpleExecutionContext::executor_type>::value);

#endif

}  //namespace batt

#endif  // BATTERIES_ASYNC_SIMPLE_EXECUTOR_IMPL_HPP
