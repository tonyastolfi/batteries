//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/async/future.hpp>
//
#include <batteries/async/future.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace {

TEST(FutureTest, AndThen)
{
    for (bool int_error : {false, true}) {
        for (bool string_error : {false, true}) {
            batt::Promise<int> int_promise;
            batt::Future<int> future_int = int_promise.get_future();

            auto future_string = [&]() -> batt::Future<std::string> {
                if (string_error) {
                    return future_int.and_then([](int /*i*/) -> batt::StatusOr<std::string> {
                        return {batt::StatusCode::kInvalidArgument};
                    });
                } else {
                    return future_int.and_then([](int i) -> batt::StatusOr<std::string> {
                        return {batt::to_string("The answer is ", i)};
                    });
                }
            }();

            EXPECT_FALSE(future_string.is_ready());

            if (int_error) {
                int_promise.set_error(batt::StatusCode::kDataLoss);

                EXPECT_TRUE(future_int.is_ready());
                EXPECT_TRUE(future_string.is_ready());
                EXPECT_EQ(future_int.await(), (batt::StatusOr<int>{batt::Status{
                                                  batt::StatusCode::kDataLoss,
                                              }}));
                EXPECT_EQ(future_string.await(), (batt::StatusOr<std::string>{batt::Status{
                                                     batt::StatusCode::kDataLoss,
                                                 }}));

            } else {
                int_promise.set_value(42);

                EXPECT_TRUE(future_int.is_ready());
                EXPECT_TRUE(future_string.is_ready());

                EXPECT_EQ(future_int.await(), (batt::StatusOr<int>{
                                                  42,
                                              }));

                if (string_error) {
                    EXPECT_EQ(future_string.await(), (batt::StatusOr<std::string>{batt::Status{
                                                         batt::StatusCode::kInvalidArgument,
                                                     }}));
                } else {
                    EXPECT_EQ(future_string.await(), (batt::StatusOr<std::string>{
                                                         std::string{"The answer is 42"},
                                                     }));
                }
            }
        }
    }
}

TEST(FutureTest, FromValue)
{
    batt::Future<int> ok_i = batt::Future<int>::from_value(7);

    ASSERT_TRUE(ok_i.is_ready());
    EXPECT_EQ(BATT_OK_RESULT_OR_PANIC(ok_i.await()), 7);

    {
        batt::Future<std::string> ok_s = batt::Future<std::string>::from_value(4, 'a');

        ASSERT_TRUE(ok_s.is_ready());
        EXPECT_THAT(BATT_OK_RESULT_OR_PANIC(ok_s.await()), ::testing::StrEq("aaaa"));
    }
    {
        batt::Future<std::string> ok_s = batt::Future<std::string>::from_value("xyz");

        ASSERT_TRUE(ok_s.is_ready());
        EXPECT_THAT(BATT_OK_RESULT_OR_PANIC(ok_s.await()), ::testing::StrEq("xyz"));
    }
}

TEST(FutureTest, FromError)
{
    batt::Future<std::string> err_s = batt::Future<std::string>::from_error(batt::StatusCode::kInternal);

    ASSERT_TRUE(err_s.is_ready());
    EXPECT_EQ(err_s.await().status(), batt::StatusCode::kInternal);
}

TEST(FutureTest, PromiseIsReady)
{
    {
        batt::Promise<int> p;
        EXPECT_FALSE(p.is_ready());

        p.set_value(4);
        EXPECT_TRUE(p.is_ready());
    }
    {
        batt::Promise<int> p2;
        EXPECT_FALSE(p2.is_ready());

        p2.set_error(batt::StatusCode::kInternal);
        EXPECT_TRUE(p2.is_ready());
    }
}

}  // namespace
