//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_SCALABLE_GRANT_HPP
#define BATTERIES_ASYNC_SCALABLE_GRANT_HPP

#include <batteries/config.hpp>
//
#include <batteries/async/handler.hpp>
#include <batteries/async/mutex.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/types.hpp>

#include <batteries/hint.hpp>
#include <batteries/optional.hpp>

#include <algorithm>
#include <mutex>

namespace batt {

class ScalableGrant;

/**
 * \brief A pool from which ScalableGrant instances are allocated.
 *
 * \see ScalableGrant
 */
class ScalableGrantIssuer
{
    friend class ScalableGrant;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   public:
    using Issuer = ScalableGrantIssuer;
    using Self = Issuer;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /**
     * \brief Constructs an empty pool.
     */
    ScalableGrantIssuer() noexcept;

    /**
     * \brief Constructs a pool with the given initial size.
     */
    explicit ScalableGrantIssuer(u64 size) noexcept;

    /** \brief ScalableGrant::Issuer is not copy-construcible.
     */
    ScalableGrantIssuer(const Self&) = delete;

    /** \brief ScalableGrant::Issuer is not copy-assignable.
     */
    Self& operator=(const Self&) = delete;

    /** \brief Destroys the pool.
     *
     * All ScalableGrant instances issued from this object MUST be released prior to destroying the
     * ScalableGrant::Issuer, or the program will panic.
     */
    ~ScalableGrantIssuer() noexcept;

    //----- --- -- -  -  -   -

    /** \brief Allocate a portion of the pool to create a new ScalableGrant.
     *
     * This function may block or not depending on the value of `wait_for_resource`.
     *
     * \return The newly allocated ScalableGrant if successful; `batt::StatusCode::kGrantUnavailable` if
     * `wait_for_resource` is false and there is not enought count in the pool to satisfy the request.
     */
    StatusOr<ScalableGrant> issue_grant(u64 count, WaitForResource wait_for_resource) noexcept;

    /**
     * \brief Increase the pool size by the specified amount.
     */
    void grow(u64 count) noexcept;

    /**
     * \brief Shut down the pool, denying all future issue_grant requests.
     */
    void close() noexcept;

    /**
     * \brief The current count available for allocation via issue_grant.
     */
    u64 available() const noexcept
    {
        return this->state_.get_count();
    }

    /** \brief The total count ever added to this pool (initial_count + grow(...)).
     */
    u64 total_size() const noexcept
    {
        return this->total_size_.load();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    struct ConsumerBase;

    /** \brief A handler waiting to consume count.
     */
    using Consumer = BasicAbstractHandler</*Base=*/ConsumerBase, /*Args...=*/StatusOr<ScalableGrant>>;

    /** \brief Generic implementation of Consumer based on concrete callable handler type `HandlerFn`.
     */
    template <typename HandlerFn>
    using ConsumerImpl =
        BasicHandlerImpl<HandlerFn, /*Base=*/ConsumerBase, /*Args...=*/StatusOr<ScalableGrant>>;

    /** \brief Base for consumer handler type; adds list node and count needed.
     */
    struct ConsumerBase
        : boost::intrusive::slist_base_hook<boost::intrusive::cache_last<true>,
                                            boost::intrusive::constant_time_size<true>> {
        u64 needed = 0;
    };

    /** \brief A list of handlers waiting to consume count.
     */
    using ConsumerList = boost::intrusive::slist<Consumer, boost::intrusive::cache_last<true>,
                                                 boost::intrusive::constant_time_size<true>>;

    /** \brief Implements the common mechanisms shared by Issuer and Grant.
     */
    class State
    {
       public:
        //+++++++++++-+-+--+----- --- -- -  -  -   -

        /** \brief Consumes the passed `to_wake` list, resolving each handler with a freshly issued grant (if
         * `status` is ok), or the passed error status.
         */
        static void wake_all(Issuer* issuer, ConsumerList& to_wake, Status status);

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        /** \brief Create a new State with the specified initial count.
         */
        explicit State(u64 init_count) noexcept;

        /** \brief Transfers all waiters and count from `that` to newly created State.
         */
        State(State&& that) noexcept;

        //----- --- -- -  -  -   -

        /** \brief Transfer `n` count to the caller, possibly blocking (if `wait_for_resource` is true).
         */
        StatusOr<ScalableGrant> acquire(Issuer* issuer, u64 n, WaitForResource wait_for_resource) noexcept;

        /** \brief Attempt a fast, non-blocking acquisition of count from this State; returns true iff `n` was
         * successfully subtracted from this State's count.
         */
        bool try_acquire(u64 n) noexcept;

        /** \brief Invokes `handler` as soon as `n` count can be granted.  May invoke `handler` before
         * returning.
         */
        template <typename HandlerFn = void(StatusOr<ScalableGrant>)>
        void async_acquire(Issuer* issuer, u64 n, HandlerFn&& handler) noexcept;

        /** \brief Acquires all count.
         */
        u64 acquire_all() noexcept
        {
            const u64 observed = this->count_.exchange(0);
            if (observed == 0) {
                std::unique_lock<std::mutex> lock{this->mutex_};
                return this->count_.exchange(0);
            }
            return observed;
        }

        /** \brief Increases the count of this State by `n`, returning a list of consumers whose handlers can
         * be invoked (success case).
         */
        BATT_WARN_UNUSED_RESULT ConsumerList release(u64 n) noexcept;

        /** \brief Closes this State, unblocking any current waiters and preventing any future waiters from
         * blocking.  The returned list of waiters should be resolved with StatusCode::kClosed or some more
         * appropriate error status.
         */
        BATT_WARN_UNUSED_RESULT ConsumerList close() noexcept;

        /** \brief Returns the current count.
         */
        u64 get_count() const noexcept
        {
            const u64 observed = this->count_.load();
            if (observed == 0) {
                std::unique_lock<std::mutex> lock{this->mutex_};
                return this->count_.load();
            }
            return observed;
        }

        /** \brief Returns true iff this State is closed, meaning that it can't be waited on.
         */
        bool is_closed() const noexcept
        {
            std::unique_lock<std::mutex> lock{this->mutex_};
            return this->closed_;
        }

        /** \brief Swap this State with `that`.
         */
        void swap(State& that) noexcept;

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        /** \brief The count held by this State.
         */
        std::atomic<u64> count_;

        mutable std::mutex mutex_;
        ConsumerList waiters_;
        bool closed_;
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Called by ScalableGrant to return spent count to the pool.
     */
    void recycle(u64 n) noexcept
    {
        ConsumerList to_wake = this->state_.release(n);
        State::wake_all(this, to_wake, OkStatus());
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    State state_;
    std::atomic<u64> total_size_;
};

/**
 * \brief A claim on some counted resource.
 *
 * The unit of a Grant's size is not specified and depends on the application context.
 *
 * This class replaces SpinGrant (the prior implementation of the `batt::Grant` type) with a new
 * implementation that is at least as fast in the no-contention case, and scales gracefully as pressure to
 * issue grants or spend parts of existing grants (blocking) increases (in terms of number of waiting Tasks).
 *
 * In contrast, SpinGrant suffers from "thundering herd" problems as the number of Tasks waiting to claim some
 * count increases, significantly degrading performance under heavy contention.
 *
 * \see ScalableGrant::Issuer
 */
class ScalableGrant
{
    friend class ScalableGrantIssuer;
    friend class ScalableGrantIssuer::State;

   public:
    using Issuer = ScalableGrantIssuer;
    using State = Issuer::State;

    ScalableGrant() noexcept;

    /** \brief ScalableGrant is not copy-constructible.
     */
    ScalableGrant(const ScalableGrant&) = delete;

    /** \brief ScalableGrant is not copy-assignable.
     */
    ScalableGrant& operator=(const ScalableGrant&) = delete;

    /** \brief ScalableGrant is move-constructible.
     */
    ScalableGrant(ScalableGrant&& that) noexcept;

    /** \brief ScalableGrant is not move-assignable.
     */
    ScalableGrant& operator=(ScalableGrant&&) = delete;

    /**
     * \brief Destroys the ScalableGrant, releasing its allocation back to the ScalableGrant::Issuer that
     * created it.
     */
    ~ScalableGrant() noexcept;

    //----- --- -- -  -  -   -

    /**
     * \brief The ScalableGrant::Issuer from which this ScalableGrant was created.
     */
    const Issuer* get_issuer() const noexcept
    {
        return this->issuer_.get();
    }

    /** \brief Tests whether `this->size()` is 0.
     */
    bool empty() const noexcept
    {
        return this->size() == 0;
    }

    /** \brief Equivalent to this->is_valid().
     */
    explicit operator bool() const noexcept
    {
        return this->is_valid();
    }

    /** \brief Tests whether this ScalableGrant has non-zero size and is connected to an
     * ScalableGrant::Issuer.  A ScalableGrant that has been moved from is no longer valid.
     */
    bool is_valid() const noexcept
    {
        return this->size() != 0 && this->issuer_;
    }

    /** \brief Tests whether revoke has been called on this ScalableGrant.
     */
    bool is_revoked() const noexcept
    {
        return this->state_.is_closed();
    }

    //----- --- -- -  -  -   -
    // All of the following public methods are thread-safe with respect to each other; they MUST NOT be called
    // concurrent to:
    //  - `SpinGrant::~SpinGrant()`
    //  - `SpinGrant other = std::move(*this);`
    //----- --- -- -  -  -   -

    /** Permanently invalidates this ScalableGrant, waking all waiters with error status.
     */
    void revoke() noexcept;

    /** The current count available for spending on this ScalableGrant.
     */
    u64 size() const noexcept
    {
        return this->state_.get_count();
    }

    /** Spends part of the grant, returning a new ScalableGrant representing the spent amount if successful;
     * otherwise:
     *   - `batt::StatusCode::kGrantUnavailable` if the remaining size of this grant isn't big enough
     *   - `batt::StatusCode::kGrantRevoked` if this ScalableGrant has been revoked
     *   - `batt::StatusCode::kFailedPrecondition` if this ScalableGrant has been invalidated by a move
     */
    StatusOr<ScalableGrant> spend(u64 count,
                                  WaitForResource wait_for_resource = WaitForResource::kFalse) noexcept;

    /** Spends all of the grant, returning the previous size.
     */
    u64 spend_all() noexcept;

    /** Increases this grant by that.size() and set that to empty.
     *
     * Will panic unless all of the following are true:
     *    - this->get_issuer() != nullptr
     *    - this->get_issuer() == that.get_issuer()
     */
    ScalableGrant& subsume(ScalableGrant&& that) noexcept;

    /** Swaps the values of this and that.
     */
    void swap(ScalableGrant& that) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    ScalableGrant(Issuer* issuer, u64 count) noexcept;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    // Tracks the currently held count, the closed/revoked state, and any waiters.
    //
    State state_;

    // This field *must not* change after it is initialized.
    //
    UniqueNonOwningPtr<Issuer> issuer_;
};

inline std::ostream& operator<<(std::ostream& out, const ScalableGrant& t)
{
    return out << "Grant{.size=" << t.size() << ",}";
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename HandlerFn /*= void(StatusOr<ScalableGrant>)*/>
void ScalableGrantIssuer::State::async_acquire(Issuer* issuer, u64 n, HandlerFn&& handler) noexcept
{
    bool ready = false;
    bool closed = false;
    {
        std::unique_lock<std::mutex> lock{this->mutex_};

        if (this->closed_) {
            ready = true;
            closed = true;
        } else {
            u64 local_count = this->count_.exchange(0);
            auto on_scope_exit = batt::finally([&] {
                this->count_.store(local_count);
            });

            if (local_count >= n) {
                local_count -= n;
                ready = true;
            } else {
                Consumer* client = ConsumerImpl<std::decay_t<HandlerFn>>::make_new(BATT_FORWARD(handler));
                client->needed = n;
                this->waiters_.push_back(*client);
            }
        }
    }

    if (ready) {
        if (!closed) {
            handler(StatusOr<ScalableGrant>{ScalableGrant{issuer, n}});
        } else {
            handler(StatusOr<ScalableGrant>{Status{StatusCode::kClosed}});
        }
    }
}

}  //namespace batt

#if BATT_HEADER_ONLY
#include <batteries/async/scalable_grant_impl.hpp>
#endif

#endif  // BATTERIES_ASYNC_SCALABLE_GRANT_HPP
