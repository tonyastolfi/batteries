//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/fake_time_service.hpp>
//
#include <batteries/async/fake_time_service_impl.hpp>

#endif  // !BATT_HEADER_ONLY
