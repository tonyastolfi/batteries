//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/latch_decl.hpp>
//
#include <batteries/async/latch_impl.ipp>
//
#include <batteries/async/latch_impl.hpp>

#endif  // !BATT_HEADER_ONLY
