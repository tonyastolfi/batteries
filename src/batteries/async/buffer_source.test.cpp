//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2022 Anthony Paul Astolfi
//
#include <batteries/async/buffer_source.hpp>
//
#include <batteries/async/buffer_source.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/stream_buffer.hpp>
#include <batteries/seq/boxed.hpp>

namespace {

using namespace batt::int_types;

TEST(BufferSourceTest, HasConstBufferSequenceRequirementsTest)
{
    EXPECT_TRUE((batt::HasConstBufferSequenceRequirements<std::vector<batt::ConstBuffer>>{}));
    EXPECT_TRUE((batt::HasConstBufferSequenceRequirements<std::vector<batt::MutableBuffer>>{}));
    EXPECT_TRUE((batt::HasConstBufferSequenceRequirements<std::array<batt::MutableBuffer, 2>>{}));
    EXPECT_FALSE((batt::HasConstBufferSequenceRequirements<int>{}));
    EXPECT_FALSE((batt::HasConstBufferSequenceRequirements<batt::BoxedSeq<std::pair<int, std::string>>>{}));
}

TEST(BufferSourceTest, HasBufferSourceRequirementsTest)
{
    EXPECT_TRUE((batt::HasBufferSourceRequirements<batt::StreamBuffer>{}));
    EXPECT_FALSE((batt::HasBufferSourceRequirements<batt::BoxedSeq<batt::ConstBuffer>>{}));
}

TEST(BufferSourceTest, TakePrefix)
{
    constexpr usize kBufferSize = 16;
    const std::string_view kTestData{"0123456789"};

    for (usize offset = 0; offset < kBufferSize; ++offset) {
        for (usize prefix_size = 0; prefix_size <= kTestData.size() + 2; ++prefix_size) {
            batt::StreamBuffer sb{kBufferSize};

            if (offset > 0) {
                ASSERT_TRUE(sb.prepare_exactly(offset).ok());
                sb.commit(offset);
                sb.consume(offset);
            }

            batt::BufferSource sb_prefix = sb | batt::seq::take_n(prefix_size);

            ASSERT_TRUE(sb.write_all(batt::ConstBuffer{kTestData.data(), kTestData.size()}).ok());

            sb.close_for_write();

            auto data = sb_prefix.fetch_at_least(1);

            if (prefix_size == 0) {
                EXPECT_EQ(data.status(), batt::StatusCode::kEndOfStream);
            } else {
                ASSERT_TRUE(data.ok()) << BATT_INSPECT(data.status()) << BATT_INSPECT(sb.size())
                                       << BATT_INSPECT(sb_prefix.size());
                EXPECT_EQ(boost::asio::buffer_size(*data), std::min(prefix_size, kTestData.size()));

                batt::StatusOr<std::vector<char>> bytes = sb_prefix | batt::seq::collect_vec();

                ASSERT_TRUE(bytes.ok()) << BATT_INSPECT(bytes.status());
                EXPECT_THAT((std::string_view{bytes->data(), bytes->size()}),
                            ::testing::StrEq(kTestData.substr(0, std::min(prefix_size, kTestData.size()))));
            }
        }
    }
}

TEST(BufferSourceTest, PrependBuffers)
{
    batt::StreamBuffer rest{1024};

    const std::string first_str = "A penny saved... ";
    const std::string second_str = "is a penny earned.";

    {
        batt::Status s = rest.write_all(batt::ConstBuffer{second_str.data(), second_str.size()});
        ASSERT_TRUE(s.ok()) << s;

        rest.close_for_write();
    }

    batt::BufferSource src = rest | batt::seq::prepend(batt::ConstBuffer{first_str.data(), first_str.size()});
    batt::StatusOr<std::vector<char>> bytes = src | batt::seq::collect_vec();

    ASSERT_TRUE(bytes.ok()) << bytes.status();
    EXPECT_THAT((std::string_view{bytes->data(), bytes->size()}), ::testing::StrEq(first_str + second_str));
}

TEST(BufferSourceTest, PrependBuffersEndOfStream)
{
    batt::StreamBuffer empty_stream{64};
    empty_stream.close_for_write();

    auto src = empty_stream | batt::seq::prepend(batt::ConstBuffer{});
    auto fetched = src.fetch_at_least(1);

    EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream);
}

// SingleBufferSource test plan:
//
//  1. Default construct
//  2. Construct from empty buffer
//  3. Construct from buffer size 1
//  4. Construct from medium sized buffer
//  5. Construct from void*, usize
//  6. Construct from const char* (implicit conversion to std::string_view)
//  7. Construct from std::string (implicit conversion to std::string_view)
//  8. Construct from std::string_view
//  9. Move construct
//     a. verify old object size unmodified (?)
//  10. fetch_at_least
//     a. 0 : always succeeds
//     b. > size : returns kEndOfStream
//     c. >0, <= size : succeeds with full buffer (all available data)
//  11. consume
//     a. 0 : same data available
//     b. 1...<size : advances the buffer, leaves data there
//     c. == size : fetch_at_least(1) always returns empty after
//     d. > size : same effect as == size
//  12. Construct BufferSource directly from:
//     a. SingleBufferSource
//     b. ConstBuffer
//     c. void*, usize
//     d. std::string_view
//     e. std::string
//     f. const char* (c_str)
//
TEST(BufferSourceTest, SingleBufferSource)
{
    const auto empty_source_tests = [](batt::SingleBufferSource& src) {
        for (usize i = 0; i < 2; ++i) {
            EXPECT_EQ(src.size(), 0u);
            EXPECT_EQ(src.fetch_at_least(1).status(), batt::StatusCode::kEndOfStream);

            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src.fetch_at_least(0);

            ASSERT_TRUE(fetched.ok()) << BATT_INSPECT(fetched.status());
            EXPECT_TRUE(fetched->empty());

            src.consume(100);
        }
    };

    //  1. Default construct
    //
    {
        batt::SingleBufferSource src;

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }

    //  2. Construct from empty buffer
    //
    {
        batt::SingleBufferSource src{batt::ConstBuffer{nullptr, 0}};

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }
    {
        batt::SingleBufferSource src{batt::ConstBuffer{"123", 3}};
        src = batt::ConstBuffer{};

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }
    {
        batt::SingleBufferSource src{std::string_view{""}};

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }
    {
        batt::SingleBufferSource src{batt::ConstBuffer{"123", 3}};
        src = std::string_view{""};

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }
    {
        batt::SingleBufferSource src{nullptr, 0};

        ASSERT_NO_FATAL_FAILURE(empty_source_tests(src));
    }

    //  3. Construct from buffer size 1
    //
    {
        const char* data = "a";
        batt::SingleBufferSource src{data};

        EXPECT_EQ(src.size(), 1u);

        batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src.fetch_at_least(1);

        ASSERT_TRUE(fetched.ok()) << BATT_INSPECT(fetched.status());
        EXPECT_EQ(fetched->size(), 1u);
        EXPECT_EQ((*fetched)[0].data(), (const void*)data);
        EXPECT_EQ((*fetched)[0].size(), 1u);
    }

    static constexpr usize kData2Size = 13;
    static const char data2[kData2Size + 1 /*for null-terminator*/] = "Hello, world.";

    const auto non_empty_source_tests = [](batt::SingleBufferSource& src, int line,
                                           bool compare_str_contents = false) {
        const auto debug_info = [&](std::ostream& out) {
            out << BATT_INSPECT(line);
        };

        EXPECT_EQ(src.size(), kData2Size) << debug_info;

        for (usize i = 0; i < 2; ++i) {
            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src.fetch_at_least(1);

            ASSERT_TRUE(fetched.ok()) << BATT_INSPECT(fetched.status()) << debug_info;
            EXPECT_EQ(fetched->size(), 1u) << debug_info;
            EXPECT_EQ((*fetched)[0].size(), kData2Size) << debug_info;
            if (compare_str_contents) {
                EXPECT_THAT(
                    (std::string_view{static_cast<const char*>((*fetched)[0].data()), (*fetched)[0].size()}),
                    ::testing::StrEq(data2))
                    << debug_info;
            } else {
                EXPECT_EQ((*fetched)[0].data(), (const void*)data2) << debug_info;
            }

            if (i == 0) {
                //  11. consume
                //     a. 0 : same data available
                //
                src.consume(0);
            }
        }

        //  11. consume
        //     b. 1...<size : advances the buffer, leaves data2 there
        //
        src.consume(1);

        //  10. fetch_at_least
        //     c. >0, <= size : succeeds with full buffer (all available data2)
        //
        {
            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src.fetch_at_least(1);

            ASSERT_TRUE(fetched.ok()) << BATT_INSPECT(fetched.status()) << debug_info;
            EXPECT_EQ(fetched->size(), 1u) << debug_info;
            if (compare_str_contents) {
                EXPECT_THAT(
                    (std::string_view{static_cast<const char*>((*fetched)[0].data()), (*fetched)[0].size()}),
                    ::testing::StrEq(data2 + 1));
            } else {
                EXPECT_EQ((*fetched)[0].data(), (const void*)(data2 + 1)) << debug_info;
            }
            ASSERT_EQ((*fetched)[0].size(), kData2Size - 1) << debug_info;
            EXPECT_EQ(boost::asio::buffer_size(*fetched), kData2Size - 1) << debug_info;
        }

        //  11. consume
        //     c. == size : fetch_at_least(1) always returns empty after
        //
        batt::SingleBufferSource src2 = src;
        src2.consume(kData2Size - 1);
        {
            EXPECT_EQ(src2.size(), 0u);

            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src2.fetch_at_least(1);

            EXPECT_FALSE(fetched.ok()) << debug_info;
            EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream) << debug_info;
        }

        batt::SingleBufferSource src3 = std::move(src);

        //  9. Move construct
        //     a. verify old object size unmodified (?)
        //
        EXPECT_EQ(src.size(), kData2Size - 1);
        EXPECT_EQ(src3.size(), kData2Size - 1);

        // 13. close_for_read() : has same effect as consuming all
        //
        src3.close_for_read();
        {
            EXPECT_EQ(src3.size(), 0u);

            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src3.fetch_at_least(1);

            EXPECT_FALSE(fetched.ok()) << debug_info;
            EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream) << debug_info;
        }

        //  11. consume
        //     d. > size : same effect as == size
        //
        src.consume(kData2Size + 10);
        {
            EXPECT_EQ(src2.size(), 0u);

            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 1>> fetched = src2.fetch_at_least(1);

            EXPECT_FALSE(fetched.ok()) << debug_info;
            EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream) << debug_info;
        }
    };

    //  4. Construct from medium sized buffer
    //
    {
        batt::SingleBufferSource src{batt::ConstBuffer{data2, kData2Size}};

        ASSERT_NO_FATAL_FAILURE(non_empty_source_tests(src, __LINE__));
    }

    //  5. Construct from void*, usize
    //
    {
        batt::SingleBufferSource src{(const void*)data2, kData2Size};

        ASSERT_NO_FATAL_FAILURE(non_empty_source_tests(src, __LINE__));
    }

    //  6. Construct from const char* (implicit conversion to std::string_view)
    //
    {
        batt::SingleBufferSource src{data2};  // implicit const char* -> std::string_view

        ASSERT_NO_FATAL_FAILURE(non_empty_source_tests(src, __LINE__));
    }

    //  7. Construct from std::string (implicit conversion to std::string_view)
    //
    {
        auto str = std::string{data2};
        batt::SingleBufferSource src{str};

        ASSERT_NO_FATAL_FAILURE(non_empty_source_tests(src, __LINE__, /*compare_str_contents=*/true));
    }

    //  8. Construct from std::string_view
    //
    {
        batt::SingleBufferSource src{std::string_view{data2}};

        ASSERT_NO_FATAL_FAILURE(non_empty_source_tests(src, __LINE__));
    }

    //  12. Construct BufferSource directly from:
    //     a. SingleBufferSource
    //
    {
        batt::BufferSource src{batt::SingleBufferSource{data2, kData2Size}};

        EXPECT_EQ(src.size(), kData2Size);
    }

    //  12. Construct BufferSource directly from:
    //     b. ConstBuffer
    //
    {
        batt::BufferSource src{batt::ConstBuffer{data2, kData2Size}};

        EXPECT_EQ(src.size(), kData2Size);
    }

    //  12. Construct BufferSource directly from:
    //     c. void*, usize
    //
    {
        batt::BufferSource src{data2, kData2Size};

        EXPECT_EQ(src.size(), kData2Size);
    }

    //  12. Construct BufferSource directly from:
    //     d. std::string_view
    //
    {
        batt::BufferSource src{std::string_view{data2, kData2Size}};

        EXPECT_EQ(src.size(), kData2Size);
    }

    //  12. Construct BufferSource directly from:
    //     e. std::string
    //
    {
        batt::BufferSource src{std::string{data2, kData2Size}};

        EXPECT_EQ(src.size(), kData2Size);
    }

    //  12. Construct BufferSource directly from:
    //     f. const char* (c_str)
    //
    {
        batt::BufferSource src{(const char*)data2};

        EXPECT_EQ(src.size(), kData2Size);
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
TEST(TakeNSource, Test)
{
    for (usize prefix_size : {9, 10}) {
        batt::StreamBuffer stream_buffer{256};
        batt::StatusOr<batt::SmallVec<batt::MutableBuffer, 2>> prepared = stream_buffer.prepare_exactly(10);

        ASSERT_TRUE(prepared.ok()) << BATT_INSPECT(prepared.status());
        ASSERT_EQ(prepared->size(), 1u);
        EXPECT_EQ(stream_buffer.size(), 0u);

        batt::MutableBuffer dst = prepared->front();

        ASSERT_EQ(dst.size(), 10u);
        ASSERT_NE(dst.data(), nullptr);

        std::memcpy(dst.data(), "0123456789", 10);
        stream_buffer.commit(10);

        EXPECT_EQ(stream_buffer.size(), 10u);

        batt::BufferSource src{stream_buffer | batt::seq::take_n(prefix_size)};
        {
            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 2>> fetched =
                src.fetch_at_least(prefix_size + 1);

            EXPECT_FALSE(fetched.ok());
            EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream);
        }

        std::ostringstream oss;
        batt::Status status = src | batt::seq::print_out(oss);

        EXPECT_TRUE(status.ok()) << BATT_INSPECT(status);
        if (prefix_size == 9) {
            EXPECT_THAT(oss.str(), ::testing::StrEq("012345678"));
        } else {
            BATT_CHECK_EQ(prefix_size, 10u);
            EXPECT_THAT(oss.str(), ::testing::StrEq("0123456789"));
        }

        {
            batt::StatusOr<batt::SmallVec<batt::ConstBuffer, 2>> fetched = src.fetch_at_least(1);

            EXPECT_FALSE(fetched.ok());
            EXPECT_EQ(fetched.status(), batt::StatusCode::kEndOfStream);
        }
    }
}

}  // namespace
