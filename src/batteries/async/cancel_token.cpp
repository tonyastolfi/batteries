//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/cancel_token.hpp>
//
#include <batteries/async/cancel_token_impl.hpp>

#endif  // !BATT_HEADER_ONLY
