//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_MUTEX_HPP
#define BATTERIES_ASYNC_MUTEX_HPP

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/int_types.hpp>
#include <batteries/pointers.hpp>

#include <atomic>

namespace batt {

class LockBase;

/** \brief Base class of typed Mutex<T>; implements core logic without any type dependencies.
 */
class MutexBase
{
   public:
    //+++++++++++-+-+--+----- --- -- -  -  -   -

    friend class LockBase;

   protected:
    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief The most-recent end of the queue of locks; once a locker has acquired the mutex, it stays in
     * this queue until it passes the lock to the next in line.
     */
    mutable std::atomic<void*> queue_back_{nullptr};
};

class LockBase
{
   public:
    /** \brief The maximum number of locks a thread can hold at a time, if using the legacy Mutex<T>::lock()
     * API.
     */
    static constexpr usize kMaxLockDepth = 8;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    struct PerThreadState;

    static PerThreadState& per_thread();

    static LockBase* acquire(const MutexBase& mutex);

    static void release(LockBase* lock);

    /** \brief The possible local states for a lock.
     */
    enum struct State {

        /** \brief This lock does not hold the mutex.
         */
        kUnlocked = 0,

        /** \brief This lock holds the mutex; it is not known whether it is still at the back of the mutex
         * queue, nor whether its next pointer has been set.
         */
        kLocked = 1,

        /** \brief This lock holds the mutex; it is known that it is no longer at the back of the mutex queue,
         * and that its next pointer has been set.
         */
        kLockedHaveNext = 2,

        kInvalid = 3,
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Constructs a lock object in an unlocked state.
     */
    LockBase() noexcept
        : mutex_{nullptr}
        , state_{State::kUnlocked}
        , next_{nullptr}
        , waiting_to_acquire_{false}
    {
    }

    /** \brief Constructs a lock object, returning from the constructor only after having acquired the mutex.
     */
    explicit LockBase(const MutexBase& mutex) noexcept
        : mutex_{std::addressof(mutex)}
        , state_{State::kUnlocked}
        , next_{nullptr}
        , waiting_to_acquire_{false}
    {
        // IMPORTANT: this should be the only call to acquire_impl!  Please do not add a public `lock()`
        // member function (or equivalent) without completely reviewing the design of this entire class and
        // the algorithms it uses.
        //
        this->acquire_impl();
    }

    /** \brief Moves the passed lock into a newly constructed lock object, retaining its locked state.
     */
    LockBase(LockBase&& src) noexcept
        : mutex_{nullptr}
        , state_{State::kUnlocked}
        , next_{nullptr}
        , waiting_to_acquire_{false}
    {
        this->move_impl(std::move(src));
    }

    /** \brief Locks may not be copy-constructed.
     */
    LockBase(const LockBase&) = delete;

    /** \brief Locks may not be copy-assigned.
     */
    LockBase& operator=(const LockBase&) = delete;

    /** \brief Destroys this lock object, releasing the mutex if it is currently held by this.
     */
    ~LockBase() noexcept
    {
        this->release_impl();
    }

    /** \brief Releases any mutex currently held by this, and moves `src` into this, retaining its locked
     * state.
     */
    LockBase& operator=(LockBase&& src) noexcept
    {
        this->release_impl();
        this->move_impl(std::move(src));

        return *this;
    }

    /** \brief Releases the mutex, if held; safe to call if this lock object isn't currently holding the
     * mutex.
     */
    void release() noexcept
    {
        this->release_impl();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    void acquire_impl() noexcept
    {
        {
            void* prior_value = this->mutex_->queue_back_.exchange(this);

            if (prior_value == nullptr) {
                this->state_ = State::kLocked;
                return;
            }

            //----- --- -- -  -  -   -
            // This is the only section of code where waiting_to_acquire_ is true.
            //
            this->waiting_to_acquire_->set_value(true);
            //
            LockBase* prev_lock = static_cast<LockBase*>(prior_value);
            prev_lock->next_->set_value_and_release(this);
            //
            // NOTE: this call to `set_value_and_release` MUST be the final usage of the pointer value read
            // into `prior_value` above.
        }
        //
        BATT_CHECK_OK(this->waiting_to_acquire_->await_equal(false));
        //
        //----- --- -- -  -  -   -

        this->state_ = State::kLocked;
    }

    void move_impl(LockBase&& src) noexcept
    {
        this->mutex_ = src.mutex_;
        this->state_ = src.state_;

        // We will transfer the value of src.next_ below, if necessary.
        //
        this->next_->set_value(nullptr);

        // `src.waiting_to_acquire_` should *always* be false, since being true would imply that src is being
        // moved while inside a call to acquire, meaning it is partially constructed, so how exactly is it
        // being moved??
        //
        BATT_ASSERT(!src.waiting_to_acquire_->get_value());
        this->waiting_to_acquire_->set_value(false);

        // If src wasn't locked, then we're done!
        //
        if (this->state_ == State::kUnlocked) {
            return;
        }

        // Let src know it no longer holds the mutex.
        //
        src.state_ = State::kUnlocked;

        if (this->state_ == State::kLocked) {
            void* expected_value = std::addressof(src);
            for (;;) {
                // Try to atomically swap `this` into the mutex `queue_back_` where `&src` was; if we succeed,
                // then no one will ever know the move took place, and we are done!
                //
                // NOTE: we know that if there is a LockBase object whose address is held by
                // MutexBase::queue_back_, it MUST be `src`, since we have an rvalue reference to `src`,
                // therefore `src` can't have been destroyed and its memory reclaimed (yet).
                //
                if (this->mutex_->queue_back_.compare_exchange_weak(expected_value, this)) {
                    BATT_ASSERT_EQ(src.next_->get_value(), nullptr);
                    return;
                }

                // If this was a spurious failure, just retry.
                //
                if (expected_value == std::addressof(src)) {
                    continue;
                }
                break;
            }

            // If we get here, there is some lock who came _after_ `src` to acquire the mutex;  that lock
            // is obligated to set `src.next_` to itself so it can be unblocked when the lock is released.
            // Since `this` is the new owner of the lock, it is now responsible for signaling `src.next_`,
            // so wait for `src.next_` to be set and copy the pointer to `this->next_`.
            //
            StatusOr<LockBase*> status_or_next = src.next_->await_not_equal(nullptr);
            BATT_CHECK_OK(status_or_next);

            LockBase* const src_next = *status_or_next;
            BATT_ASSERT_NOT_NULLPTR(src_next);

            this->next_->set_value(src_next);
            this->state_ = State::kLockedHaveNext;

        } else {
            // Not kUnlocked, not kLocked; kLockedHaveNext is the only remaining possibility.
            //
            BATT_ASSERT_EQ(this->state_, State::kLockedHaveNext);

            LockBase* const src_next = src.next_->get_value();

            // kLockedHaveNext implies that src.next_ is non-null.
            //
            BATT_CHECK_NOT_NULLPTR(src_next) << BATT_INSPECT((void*)this) << BATT_INSPECT((void*)src_next);
            this->next_->set_value(src_next);
        }
    }

    void release_impl() noexcept
    {
        if (this->state_ == State::kUnlocked) {
            return;
        }

        const State prior_state = this->state_;
        this->state_ = State::kUnlocked;

        LockBase* next_lock;  // MUST be set in both branches below.

        if (prior_state == State::kLocked) {
            void* expected_value = this;
            for (;;) {
                if (this->mutex_->queue_back_.compare_exchange_weak(expected_value, nullptr)) {
                    return;
                }

                // If this was a spurious failure, just retry.
                //
                if (expected_value == this) {
                    continue;
                }
                break;
            }

            StatusOr<LockBase*> status_or_next = this->next_->await_not_equal(nullptr);
            BATT_CHECK_OK(status_or_next);
            next_lock = *status_or_next;

        } else {
            BATT_ASSERT_EQ(prior_state, State::kLockedHaveNext);
            next_lock = this->next_->get_value();
        }

        BATT_ASSERT_NOT_NULLPTR(next_lock) << BATT_INSPECT(prior_state);
        next_lock->waiting_to_acquire_->set_value_and_release(false);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    const MutexBase* mutex_;
    State state_;
    CpuCacheLineIsolated<Watch<LockBase*>> next_;
    CpuCacheLineIsolated<Watch<bool>> waiting_to_acquire_;
};

template <typename T>
class ScopedLock;

/** \brief Provides mutually-exclusive access to an instance of type `T`.
 *
 * This class has two advantages over `std::mutex`:
 *     1. It will yield the current batt::Task (if there is one) when blocking to acquire a lock, allowing
 *        the current thread to be used by other tasks
 *     2. By embedding the protected type `T` within the object, there is a much lower chance that state
 * which should be accessed via a mutex will accidentally be accessed directly
 *
 * This mutex implementation is mostly fair because it uses a modified version of [Lamport's Bakery
 * Algorithm](https://en.wikipedia.org/wiki/Lamport's_bakery_algorithm).  It is non-recursive, so
 * threads/tasks that attempt to acquire a lock that they already have will deadlock.  Also, an attempt to
 * acquire a lock on a batt::Mutex can't be cancelled, so it is not possible to set a timeout on lock
 * acquisition.
 */
template <typename T>
class Mutex : public MutexBase
{
   public:
    template <typename>
    friend class ScopedLock;

#if BATT_MUTEX_NO_LEGACY_API

    using Lock = ScopedLock<T>;
    using ConstLock = ScopedLock<const T>;

#else  // BATT_MUTEX_NO_LEGACY_API

    /** \brief Represents a lock aquisition.
     */
    template <typename U, typename MutexT>
    class LockImpl
    {
       public:
        /** \brief Acquire a lock on the passed Mutex.
         */
        explicit LockImpl(MutexT& m) noexcept : lock_base_{m}, val_{std::addressof(m.value_)}
        {
        }

        /** \brief Lock is not copy-constructible.
         */
        LockImpl(const LockImpl&) = delete;

        /** \brief Lock is not copy-assignable.
         */
        LockImpl& operator=(const LockImpl&) = delete;

        /** \brief Lock is move-constructible.
         */
        LockImpl(LockImpl&&) = default;

        /** \brief Lock is move-assignable.
         */
        LockImpl& operator=(LockImpl&&) = default;

        /** \brief Destroy the Lock object, releasing the Mutex.
         */
        ~LockImpl() noexcept
        {
            this->release();
        }

        /** \brief Test whether this Lock object is the current holder of an exclusive lock on the
         * underlying Mutex.
         */
        bool is_held() const noexcept
        {
            return this->val_ != nullptr;
        }

        /** \brief Equivalent to this->is_held().
         */
        explicit operator bool() const noexcept
        {
            return this->is_held();
        }

        /** \brief Access the locked object.  WARNING: Behavior is undefined unless this->is_held() is
         * true.
         */
        U& operator*() noexcept
        {
            return *this->val_;
        }

        /** \brief Access the locked object by pointer.
         */
        U* get() noexcept
        {
            return this->val_.get();
        }

        /** \brief Access the locked object by reference.
         */
        U& value() noexcept
        {
            return *this->val_;
        }

        /** \brief Access members of the locked object.
         */
        U* operator->() noexcept
        {
            return this->val_.get();
        }

        /** \brief Explicitly release this lock.
         */
        bool release() noexcept
        {
            if (this->val_ != nullptr) {
                this->val_.release();
                this->lock_base_.release();
                return true;
            }
            return false;
        }

       private:
        LockBase lock_base_;

        // Direct pointer to the locked object; if nullptr, this indicates that the lock has been
        // released.
        //
        UniqueNonOwningPtr<U> val_;
    };

    /** \brief Lock guard for mutable access.
     */
    using Lock = LockImpl<T, Mutex>;

    /** \brief Lock guard for const access.
     */
    using ConstLock = LockImpl<const T, const Mutex>;

#endif  // BATT_MUTEX_NO_LEGACY_API

    /** \brief Returned by Mutex::thread_safe_base when no-lock access isn't enabled; the name of this
     * type is designed to produce a compilation error that makes it obvious what the problem is.
     */
    template <typename>
    struct ThreadSafeBaseIsNotSupportedByType {
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief (INTERNAL USE ONLY) Return a pointer to the thread-safe base class of the protected object.
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase>
    static Base* thread_safe_base(Self* ptr)
    {
        return ptr;
    }

    /** \brief (INTERNAL USE ONLY) Return a const pointer to the thread-safe base class of the protected
     * object.
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase>
    static const Base* thread_safe_base(const Self* ptr)
    {
        return ptr;
    }

    /** \brief (INTERNAL USE ONLY) Return a pointer to the thread-safe base class of the protected
     * object (std::unique_ptr variant).
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase, typename = void>
    static Base* thread_safe_base(const std::unique_ptr<Self>* ptr)
    {
        return ptr->get();
    }

    /** \brief (INTERNAL USE ONLY) Overload that is selected in the "not supported" case - designed to
     * produce an error message that elucidates the root cause of the problem.
     */
    static ThreadSafeBaseIsNotSupportedByType<T>* thread_safe_base(...)
    {
        return nullptr;
    }

    /** \brief Mutex is not copy-constructible.
     */
    Mutex(const Mutex&) = delete;

    /** \brief Mutex is not copy-assignable.
     */
    Mutex& operator=(const Mutex&) = delete;

    /** \brief Default-initializes the protected object.
     */
    Mutex() = default;

    /** \brief Initializes the protected object by forwarding the args to T's constructor.
     */
    template <typename... Args, typename = EnableIfNoShadow<Mutex, Args...>>
    explicit Mutex(Args&&... args) noexcept : value_(BATT_FORWARD(args)...)
    {
    }

#if !BATT_MUTEX_NO_LEGACY_API

    /** \brief Acquires a lock on the protected object.
     */
    Lock lock()
    {
        return Lock{*this};
    }

    /** \brief Acquires a lock on a const reference to the protected object, for read-only access.
     */
    ConstLock lock() const
    {
        return ConstLock{*this};
    }

#endif  // BATT_MUTEX_NO_LEGACY_API

    /** \brief Performs the specified action while holding a lock on the Mutex, by passing a reference to the
     * protected object to the specified action.
     *
     * This function first locks the Mutex, guaranteeing that the current Task will have exclusive access to
     * the protected object (T).  Then it passes a non-const reference to the supplied function, whose
     * signature should be `Result (T& obj)`.  When `action` returns, the Mutex lock is released, and the
     * return value of `action` is passed back to the caller.
     *
     * \return The value returned by `action`.
     */
    template <typename Action>
    decltype(auto) with_lock(Action&& action);

    /** \brief Accesses the protected object's thread-safe base class members.
     */
    auto operator->()
    {
        return thread_safe_base(&this->value_);
    }

    /** \brief Accesses the protected object's thread-safe base class by reference.
     */
    decltype(auto) no_lock()
    {
        return *thread_safe_base(&this->value_);
    }

    /** \brief Accesses the protected object's thread-safe base class by pointer.
     */
    decltype(auto) no_lock() const
    {
        return *thread_safe_base(&this->value_);
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    //+++++++++++-+-+--+----- --- -- -  -  -   -

    T value_;
};

template <typename T>
class ScopedLock
{
   public:
    /** \brief Acquire a lock on the passed Mutex.
     */
    explicit ScopedLock(Mutex<T>& m) noexcept : lock_base_{m}, val_{std::addressof(m.value_)}
    {
    }

    /** \brief Acquire a lock on the passed Mutex.
     */
    explicit ScopedLock(const Mutex<std::remove_const_t<T>>& m) noexcept
        : lock_base_{m}
        , val_{const_cast<std::add_const_t<T>*>(std::addressof(m.value_))}
    {
    }

    /** \brief Lock is not copy-constructible.
     */
    ScopedLock(const ScopedLock&) = delete;

    /** \brief Lock is not copy-assignable.
     */
    ScopedLock& operator=(const ScopedLock&) = delete;

    /** \brief Destroy the Lock object, releasing the Mutex.
     */
    ~ScopedLock() noexcept
    {
    }

    /** \brief Equivalent to this->is_held().
     */
    explicit operator bool() const noexcept
    {
        return this->is_held();
    }

    /** \brief Access the locked object.  WARNING: Behavior is undefined unless this->is_held() is
     * true.
     */
    T& operator*() noexcept
    {
        return *this->val_;
    }

    /** \brief Access the locked object by pointer.
     */
    T* get() noexcept
    {
        return this->val_.get();
    }

    /** \brief Access the locked object by reference.
     */
    T& value() noexcept
    {
        return *this->val_;
    }

    /** \brief Access members of the locked object.
     */
    T* operator->() noexcept
    {
        return this->val_.get();
    }

   private:
    LockBase lock_base_;

    // Direct pointer to the locked object; if nullptr, this indicates that the lock has been
    // released.
    //
    UniqueNonOwningPtr<T> val_;
};

#define BATT_SCOPED_LOCK(LOCK_VAR, MUTEX_EXPR)                                                               \
    decltype(MUTEX_EXPR)::Lock LOCK_VAR                                                                      \
    {                                                                                                        \
        MUTEX_EXPR                                                                                           \
    }

#define BATT_SCOPED_CONST_LOCK(LOCK_VAR, MUTEX_EXPR)                                                         \
    decltype(MUTEX_EXPR)::ConstLock LOCK_VAR                                                                 \
    {                                                                                                        \
        MUTEX_EXPR                                                                                           \
    }

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Action>
inline decltype(auto) Mutex<T>::with_lock(Action&& action)
{
    ScopedLock<T> lock{*this};
    return BATT_FORWARD(action)(this->value_);
}

}  // namespace batt

#endif  // BATTERIES_ASYNC_MUTEX_HPP

#include <batteries/config.hpp>
//
#if BATT_HEADER_ONLY
#include <batteries/async/mutex_impl.hpp>
#endif
