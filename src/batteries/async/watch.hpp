//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#include <batteries/async/watch_decl.hpp>

#include <batteries/async/watch.ipp>

#if BATT_HEADER_ONLY
#include <batteries/async/watch_impl.hpp>
#endif
