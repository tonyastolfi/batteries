//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023-2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_READ_WRITE_LOCK_HPP
#define BATTERIES_ASYNC_READ_WRITE_LOCK_HPP

#include <batteries/config.hpp>
//

#include <batteries/assert.hpp>
#include <batteries/async/futex.hpp>
#include <batteries/async/task.hpp>
#include <batteries/async/watch.hpp>
#include <batteries/int_types.hpp>
#include <batteries/logging.hpp>

#include <atomic>
#include <bitset>
#include <ostream>

namespace batt {

struct DumpReadWriteLockState {
    u32 state_value;
};

std::ostream& operator<<(std::ostream& out, const DumpReadWriteLockState& dump);

enum struct ReadWriteLockQueueNodeClass : int {
    kReading = 0,
    kWriting = 1,
};

std::ostream& operator<<(std::ostream& out, ReadWriteLockQueueNodeClass t);

/** \brief An MCS-style fair reader/writer lock.
 */
template <template <typename> class WatchImplT, bool kPollForNext = false>
class BasicReadWriteLock
{
   public:
    using Self = BasicReadWriteLock;
    using Lock = Self;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    static constexpr u32 kBlockedMask = 0b1;
    static constexpr u32 kBlockedFalse = 0b0;
    static constexpr u32 kBlockedTrue = 0b1;
    static constexpr u32 kSuccessorMask = 0b110;
    static constexpr u32 kSuccessorNone = 0b000;
    static constexpr u32 kSuccessorReader = 0b010;
    static constexpr u32 kSuccessorWriter = 0b100;

    /** \brief Returns a state value representing the combination of the passed bit fields.
     */
    static constexpr u32 make_state(u32 is_blocked, u32 successor_class) noexcept
    {
        return (is_blocked & kBlockedMask) | (successor_class & kSuccessorMask);
    }

    /** \brief Returns true iff the blocked bit of state is set.
     */
    static constexpr bool is_blocked_from_state(u32 state) noexcept
    {
        return (state & kBlockedMask) != 0;
    }

    /** \brief Returns the masked successor class value of `state`, which can be compared to kSuccessorNone,
     * kSuccessorReader, and kSuccessorWriter.
     */
    static constexpr u32 successor_class_from_state(u32 state) noexcept
    {
        return (state & kSuccessorMask);
    }

    using DumpState = DumpReadWriteLockState;

    //----- --- -- -  -  -   -

    static constexpr u32 kInitialState = Self::make_state(Self::kBlockedTrue, Self::kSuccessorNone);

    static_assert(kInitialState == 1);

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Base class for Reader and Writer scoped lock guards.
     */
    class QueueNode
    {
       public:
        using NodeClass = ReadWriteLockQueueNodeClass;

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        explicit QueueNode(Lock* lock, NodeClass node_class) noexcept
            : lock_{lock}
            , class_{node_class}
            , next_{nullptr}
            , state_{Self::kInitialState}
        {
        }

        //----- --- -- -  -  -   -
        /** \brief The initial fast path is the same for both readers and writers: attempt to fetch-and-store
         * the lock->tail_ pointer to this; if the previous value was nullptr, then `this` is the current
         * holder of the lock (regardless whether it is a writer or a reader).
         */
        QueueNode* fast_lock_or_get_predecessor(u32& observed_state) noexcept
        {
            observed_state = Self::kInitialState;

            QueueNode* const pred = this->lock_->tail_->exchange(this);
            return pred;
        }

        //----- --- -- -  -  -   -
        /** \brief Updates this->state_ (and observed_state), clearing the blocked bit.
         */
        template <typename Mode>
        void set_unblocked(u32& observed_state, Mode mode) noexcept
        {
            observed_state = this->state_->fetch_and(~kBlockedTrue, mode) & ~kBlockedTrue;
        }

        //----- --- -- -  -  -   -
        /** \brief Spins/waits for the blocked bit of this->state_ to be 0.
         */
        void await_unblocked(u32& observed_state) noexcept
        {
            BATT_DEBUG_INFO("next=" << (void*)this->next_->get_value() << BATT_INSPECT((void*)this)
                                    << BATT_INSPECT(this->class_)
                                    << " state=" << DumpState{this->state_->get_value()}
                                    << BATT_INSPECT((void*)this->lock_));

            // We have exceeded the maximum allowed initial spin cycles; use the more expensive `await_true`
            // method.
            //
            const StatusOr<u32> new_observed_state =  //
                this->state_->await_true([](u32 observed_state) {
                    return !is_blocked_from_state(observed_state);
                });

            BATT_CHECK_OK(new_observed_state);

            observed_state = *new_observed_state;
        }

        //----- --- -- -  -  -   -
        /** \brief Attempts a fast release of the lock.
         *
         * The fast path is the same for both readers and writers; the goal is to *either* replace
         * `lock->tail` as though we were never there (the uncontended case), *or* to obtain the next pointer
         * so we know the successor of this node.  Depending on whether this function is called by a reader or
         * writer, there are slightly different things to do once we know the true value of `next`.
         *
         * \return true iff there is no successor of this (implies the fast unlock succeeded).
         */
        bool fast_unlock(QueueNode*& observed_next) noexcept
        {
            observed_next = this->next_->get_value();

            if (observed_next == nullptr) {
                QueueNode* presumed_tail = this;
                do {
                    if (this->lock_->tail_->compare_exchange_weak(presumed_tail, nullptr)) {
                        // Fast-path release - this QueueNode is (still) the tail and we successfully set
                        // tail to nullptr.  Return!
                        //
                        return true;
                    }
                } while (this == presumed_tail);

                // Fast-path failed, so we know there is a successor to `this`; wait for them to set our
                // `next` pointer.
                //
                this->await_next(observed_next);
            }

            return false;
        }

        //----- --- -- -  -  -   -
        /** \brief Waits until this->next_ is non-null (indicating that the successor of this node is done
         * accessing our state).  Whether or not we will need to use this->next_ depends on the state of
         * `this`, after non-nullptr next is observed.
         */
        void await_next(QueueNode*& observed_next)
        {
            if (kPollForNext) {
                while (observed_next == nullptr) {
                    observed_next = this->next_->poll();
                }

            } else {
                const StatusOr<QueueNode*> new_observed_next = this->next_->await_not_equal(nullptr);
                BATT_CHECK(new_observed_next.ok() && *new_observed_next != nullptr)
                    << BATT_INSPECT(new_observed_next);
                observed_next = *new_observed_next;
            }
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -

        Lock* const lock_;
        NodeClass const class_;
        CpuCacheLineIsolated<WatchImplT<QueueNode*>> next_;
        CpuCacheLineIsolated<WatchImplT<u32>> state_;
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    class Reader : private QueueNode
    {
       public:
        explicit Reader(Lock& lock) noexcept : QueueNode{&lock, QueueNode::NodeClass::kReading}
        {
            this->lock_for_read_impl();
        }

        ~Reader() noexcept
        {
            this->unlock_for_read_impl();
        }

        /** \brief Returns the lock for this Reader.
         */
        Lock* lock() const noexcept
        {
            return this->lock_;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        /** \brief Acquires the lock for reading (exclude all writers, share with readers).
         */
        void lock_for_read_impl() noexcept
        {
            u32 observed_state = 0;

            QueueNode* const pred = this->fast_lock_or_get_predecessor(observed_state);

            if (pred == nullptr) {
                this->lock_->reader_count_->fetch_add(1);
                this->set_unblocked(observed_state, HoldOwnership{});

            } else {
                // If our predecessor is a writer or a blocked reader whose state we were able to update (via
                // CAS) to {successor_class = reader}, then we can count on `pred` to set our state to
                // unblocked, so set pred->next to us and await that state change.
                //
                u32 presumed_pred_state = Self::kInitialState;

                if (pred->class_ == QueueNode::NodeClass::kWriting ||
                    pred->state_->compare_exchange(presumed_pred_state,
                                                   make_state(kBlockedTrue, kSuccessorReader)))  //
                {
                    pred->next_->set_value(this, ReleaseOwnership{});
                    this->await_unblocked(observed_state);

                } else {
                    // `pred` is a non-blocked reader; that means we are also unblocked!
                    //
                    this->lock_->reader_count_->fetch_add(1);
                    pred->next_->set_value(this, ReleaseOwnership{});
                    this->set_unblocked(observed_state, HoldOwnership{});
                }
            }
            //
            // At this point, we are NOT blocked.

            if (successor_class_from_state(observed_state) == kSuccessorReader) {
                QueueNode* observed_next = this->next_->get_value();
                //----- --- -- -  -  -   -
                this->await_next(observed_next);
                //----- --- -- -  -  -   -
                this->lock_->reader_count_->fetch_add(1);
                u32 observed_next_state = 0;
                observed_next->set_unblocked(observed_next_state, ReleaseOwnership{});
            }
        }

        /** \brief Releases the lock as a reader.
         */
        void unlock_for_read_impl() noexcept
        {
            QueueNode* observed_next = nullptr;

            if (!this->fast_unlock(observed_next)) {
                //
                // At this point, observed_next is NOT nullptr.

                const u32 observed_state = this->state_->get_value();
                if (successor_class_from_state(observed_state) == kSuccessorWriter) {
                    this->lock_->next_writer_->store(observed_next);
                }
            }

            // If this QueueNode is the last reader, then it's our job to wake the next writer in the queue.
            //
            if (this->lock_->reader_count_->fetch_sub(1) == 1) {
                QueueNode* const observed_next_writer = this->lock_->next_writer_->exchange(nullptr);
                if (observed_next_writer != nullptr) {
                    u32 observed_next_writer_state = 0;
                    observed_next_writer->set_unblocked(observed_next_writer_state, ReleaseOwnership{});
                }
            }
        }
    };

    //==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
    //
    class Writer : private QueueNode
    {
       public:
        explicit Writer(Lock& lock) noexcept : QueueNode{&lock, QueueNode::NodeClass::kWriting}
        {
            this->lock_for_write_impl();
        }

        ~Writer() noexcept
        {
            this->unlock_for_write_impl();
        }

        /** \brief Returns the lock for this Reader.
         */
        Lock* lock() const noexcept
        {
            return this->lock_;
        }

        //+++++++++++-+-+--+----- --- -- -  -  -   -
       private:
        /** \brief Acquires the lock for writing (total mutual exclusion).
         */
        void lock_for_write_impl() noexcept
        {
            u32 observed_state = 0;

            QueueNode* const pred = this->fast_lock_or_get_predecessor(observed_state);

            if (pred == nullptr) {
                this->lock_->next_writer_->store(this);
                if (this->lock_->reader_count_->load() == 0 &&
                    this->lock_->next_writer_->exchange(nullptr) == this) {
                    this->set_unblocked(observed_state, HoldOwnership{});
                    return;
                }

            } else {
                // IMPORTANT: this assumes the bits 0b110 are zero!
                //
                const u32 prior_pred_state = pred->state_->fetch_or(kSuccessorWriter);
                BATT_CHECK_EQ((prior_pred_state & kSuccessorMask), kSuccessorNone);

                // IMPORTANT: updating pred->next_ must come after modifying pred->state_!
                //
                pred->next_->set_value(this, ReleaseOwnership{});
            }

            this->await_unblocked(observed_state);
        }

        /** \brief Releases the lock as a writer.
         */
        void unlock_for_write_impl() noexcept
        {
            QueueNode* observed_next = nullptr;

            if (!this->fast_unlock(observed_next)) {
                //
                // At this point, observed_next is NOT nullptr.

                if (observed_next->class_ == QueueNode::NodeClass::kReading) {
                    this->lock_->reader_count_->fetch_add(1);
                }

                // Set next->blocked to false to allow the next thread to run.
                //
                u32 observed_next_state = 0;
                observed_next->set_unblocked(observed_next_state, ReleaseOwnership{});
            }
        }
    };

    /** \brief Returns the current number of readers holding this lock.
     */
    i64 reader_count() const noexcept
    {
        return this->reader_count_->load();
    }

   private:
    CpuCacheLineIsolated<std::atomic<QueueNode*>> tail_{nullptr};
    CpuCacheLineIsolated<std::atomic<i64>> reader_count_{0};
    CpuCacheLineIsolated<std::atomic<QueueNode*>> next_writer_{nullptr};
};

using ReadWriteLock = BasicReadWriteLock</*WatchImplT=*/WatchAtomic>;

inline std::ostream& operator<<(std::ostream& out, const DumpReadWriteLockState& dump)
{
    out << std::bitset<3>{dump.state_value} << "(";

    if ((dump.state_value & ReadWriteLock::kBlockedMask) == ReadWriteLock::kBlockedTrue) {
        out << "blocked";
    } else {
        out << "unblocked";
    }

    out << ",succ=";

    switch (dump.state_value & ReadWriteLock::kSuccessorMask) {
    case ReadWriteLock::kSuccessorNone:
        out << "none";
        break;

    case ReadWriteLock::kSuccessorReader:
        out << "reader";
        break;

    case ReadWriteLock::kSuccessorWriter:
        out << "writer";
        break;

    default:
        out << "INVALID";
        break;
    }

    return out << ")";
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

// Forward-declaration.
//
template <typename T, typename LockT>
class ScopedReadWriteLockImpl;

/** \brief Mutex wrapper for a value of type T, providing exclusive read/write access and non-exclusive
 * read-only access.
 */
template <typename T>
class ReadWriteMutex
{
   public:
    template <typename, typename>
    friend class ScopedReadWriteLockImpl;

    using Self = ReadWriteMutex;

    /** \brief Returned by Mutex::thread_safe_base when no-lock access isn't enabled; the name of this
     * type is designed to produce a compilation error that makes it obvious what the problem is.
     */
    template <typename>
    struct ThreadSafeBaseIsNotSupportedByType {
    };

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief (INTERNAL USE ONLY) Return a pointer to the thread-safe base class of the protected object.
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase>
    static Base* thread_safe_base(Self* ptr)
    {
        return ptr;
    }

    /** \brief (INTERNAL USE ONLY) Return a const pointer to the thread-safe base class of the protected
     * object.
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase>
    static const Base* thread_safe_base(const Self* ptr)
    {
        return ptr;
    }

    /** \brief (INTERNAL USE ONLY) Return a pointer to the thread-safe base class of the protected
     * object (std::unique_ptr variant).
     */
    template <typename Self, typename Base = typename Self::ThreadSafeBase, typename = void>
    static Base* thread_safe_base(const std::unique_ptr<Self>* ptr)
    {
        return ptr->get();
    }

    /** \brief (INTERNAL USE ONLY) Overload that is selected in the "not supported" case - designed to
     * produce an error message that elucidates the root cause of the problem.
     */
    static ThreadSafeBaseIsNotSupportedByType<T>* thread_safe_base(...)
    {
        return nullptr;
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief ReadWriteMutex is not copy-constructible.
     */
    ReadWriteMutex(const ReadWriteMutex&) = delete;

    /** \brief ReadWriteMutex is not copy-assignable.
     */
    ReadWriteMutex& operator=(const ReadWriteMutex&) = delete;

    /** \brief Default-constructs a protected instance of T.
     */
    ReadWriteMutex() = default;

    /** \brief Initializes the protected object by forwarding the args to T's constructor.
     */
    template <typename... Args, typename = EnableIfNoShadow<Self, Args...>>
    explicit ReadWriteMutex(Args&&... args) noexcept : value_(BATT_FORWARD(args)...)
    {
    }

    /** \brief Performs the specified action while holding a reader lock on the Mutex, by passing a const
     * reference to the protected object to the specified action.
     *
     * This function first locks the Mutex, guaranteeing that the current Task will have read-only access to
     * the protected object (T).  Then it passes a non-const reference to the supplied function, whose
     * signature should be `Result (const T& obj)`.  When `action` returns, the Mutex reader lock is released,
     * and the return value of `action` is passed back to the caller.
     *
     * \return The value returned by `action`.
     */
    template <typename Action>
    decltype(auto) with_read_lock(Action&& action);

    /** \brief Performs the specified action while holding a writer lock on the Mutex, by passing a
     * reference to the protected object to the specified action.
     *
     * This function first locks the Mutex, guaranteeing that the current Task will have exclusive access to
     * the protected object (T).  Then it passes a non-const reference to the supplied function, whose
     * signature should be `Result (T& obj)`.  When `action` returns, the Mutex writer lock is released,
     * and the return value of `action` is passed back to the caller.
     *
     * \return The value returned by `action`.
     */
    template <typename Action>
    decltype(auto) with_write_lock(Action&& action);

    /** \brief Accesses the protected object's thread-safe base class members.
     */
    auto operator->()
    {
        return thread_safe_base(&this->value_);
    }

    /** \brief Accesses the protected object's thread-safe base class by reference.
     */
    decltype(auto) no_lock()
    {
        return *thread_safe_base(&this->value_);
    }

    /** \brief Accesses the protected object's thread-safe base class by pointer.
     */
    decltype(auto) no_lock() const
    {
        return *thread_safe_base(&this->value_);
    }

   private:
    ReadWriteLock lock_;
    T value_;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -

template <typename T>
using ScopedReadLock = ScopedReadWriteLockImpl<const T, ReadWriteLock::Reader>;

template <typename T>
using ScopedWriteLock = ScopedReadWriteLockImpl<T, ReadWriteLock::Writer>;

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

template <typename T, typename LockT>
class ScopedReadWriteLockImpl
{
   public:
    explicit ScopedReadWriteLockImpl(ReadWriteMutex<std::remove_const_t<T>>& mutex) noexcept
        : lock_{mutex.lock_}
        , value_{mutex.value_}
    {
    }

    ScopedReadWriteLockImpl(const ScopedReadWriteLockImpl&) = delete;
    ScopedReadWriteLockImpl& operator=(const ScopedReadWriteLockImpl&) = delete;

    ~ScopedReadWriteLockImpl() = default;

    //+++++++++++-+-+--+----- --- -- -  -  -   -

    /** \brief Access the locked object by pointer.
     */
    T* get() noexcept
    {
        return std::addressof(this->value_);
    }

    /** \brief Access the locked object by reference.
     */
    T& value() noexcept
    {
        return this->value_;
    }

    /** \brief Access members of the locked object.
     */
    T* operator->() noexcept
    {
        return this->get();
    }

    /** \brief Access the locked object.
     */
    T& operator*() noexcept
    {
        return this->value();
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
   private:
    LockT lock_;
    T& value_;
};

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Action>
inline decltype(auto) ReadWriteMutex<T>::with_read_lock(Action&& action)
{
    ScopedReadLock<T> lock{*this};
    return BATT_FORWARD(action)(*lock);
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename Action>
inline decltype(auto) ReadWriteMutex<T>::with_write_lock(Action&& action)
{
    ScopedWriteLock<T> lock{*this};
    return BATT_FORWARD(action)(*lock);
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------

inline std::ostream& operator<<(std::ostream& out, ReadWriteLockQueueNodeClass t)
{
    using NodeClass = ReadWriteLockQueueNodeClass;

    switch (t) {
    case NodeClass::kReading:
        return out << "Reading";

    case NodeClass::kWriting:
        return out << "Writing";

    default:
        break;
    }
    return out << "(invalid:" << (int)t << ")";
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_READ_WRITE_LOCK_HPP
