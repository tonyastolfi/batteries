#include <batteries/async/mcs_lock.hpp>
//
#include <batteries/async/mcs_lock.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/assert.hpp>
#include <batteries/int_types.hpp>
#include <batteries/metrics/metric_collectors.hpp>

#include <mutex>
#include <thread>
#include <vector>

namespace {

using namespace batt::int_types;

BATT_BEGIN_NO_OPTIMIZE()

void lock_it(std::mutex& m)
{
    m.lock();
}

void unlock_it(std::mutex& m)
{
    m.unlock();
}

BATT_END_NO_OPTIMIZE()

const usize kMaxThreads = std::thread::hardware_concurrency() * 5 / 2;

TEST(MutexScaleTest, StdMutex)
{
    std::cout << "n_threads,std_rate_per_second" << std::endl;
    for (usize n_threads = 1; n_threads <= kMaxThreads; n_threads += 1) {
        std::mutex mutex;
        const usize count = 1000 * 1000;
        usize from = count;
        usize to = 0;
        usize total_lock_count = 0;

        std::vector<batt::CpuCacheLineIsolated<usize>> lock_count(n_threads);
        for (auto& c : lock_count) {
            *c = 0;
        }

        std::vector<std::thread> threads;

        batt::LatencyMetric update_latency;
        batt::LatencyMetric lock_latency;
        {
            auto start_time = std::chrono::steady_clock::now();
            batt::LatencyTimer update_timer{update_latency, from};

            for (usize i = 0; i < n_threads; ++i) {
                threads.emplace_back([&, i] {
                    for (;;) {
                        //                        std::unique_lock<std::mutex> lock{mutex};
                        lock_it(mutex);

                        ++*lock_count[i];

                        if (from == 0) {
                            total_lock_count += *lock_count[i];
                            unlock_it(mutex);
                            return;
                        }

                        --from;
                        ++to;

                        unlock_it(mutex);
                    }
                });
            }

            for (std::thread& t : threads) {
                t.join();
            }

            lock_latency.update(start_time, total_lock_count);
        }
        BATT_CHECK_EQ(to, count);
        BATT_CHECK_EQ(from, 0u);

        std::cout << n_threads << "," << update_latency.rate_per_second() << ","
                  << lock_latency.rate_per_second() << std::endl;
        std::cout << batt::dump_range(lock_count) << std::endl;

        if (n_threads >= std::thread::hardware_concurrency()) {
            n_threads += 9;
        } else if (n_threads >= 10) {
            n_threads += 1;
        }
    }
}

TEST(MutexScaleTest, SpinMutex)
{
    std::cout << "n_threads,std_rate_per_second" << std::endl;
    for (usize n_threads = 1; n_threads <= kMaxThreads; n_threads += 1) {
        batt::CpuCacheLineIsolated<std::atomic<u32>> spin_lock{0};
        const usize count = 1000 * 1000;
        usize from = count;
        usize to = 0;
        usize total_lock_count = 0;

        std::vector<batt::CpuCacheLineIsolated<usize>> lock_count(n_threads);
        for (auto& c : lock_count) {
            *c = 0;
        }

        std::vector<std::thread> threads;

        batt::LatencyMetric update_latency;
        batt::LatencyMetric lock_latency;
        {
            auto start_time = std::chrono::steady_clock::now();
            batt::LatencyTimer update_timer{update_latency, from};

            for (usize i = 0; i < n_threads; ++i) {
                threads.emplace_back([&, i] {
                    for (;;) {
                        for (;;) {
                            if (!spin_lock->exchange(1)) {
                                break;
                            }
                            batt::spin_yield();
                        }
                        auto on_scope_exit = batt::finally([&] {
                            spin_lock->store(0);
                        });

                        ++*lock_count[i];

                        if (from == 0) {
                            total_lock_count += *lock_count[i];
                            return;
                        }

                        --from;
                        ++to;
                    }
                });
            }

            for (std::thread& t : threads) {
                t.join();
            }

            lock_latency.update(start_time, total_lock_count);
        }
        BATT_CHECK_EQ(to, count);
        BATT_CHECK_EQ(from, 0u);

        std::cout << n_threads << "," << update_latency.rate_per_second() << ","
                  << lock_latency.rate_per_second() << std::endl;
        std::cout << batt::dump_range(lock_count) << std::endl;

        if (n_threads >= std::thread::hardware_concurrency()) {
            n_threads += 9;
        } else if (n_threads >= 10) {
            n_threads += 1;
        }
    }
}

TEST(MutexScaleTest, McsMutex)
{
    std::cout << "kind,n_threads,mcs_rate_per_second,locks_per_cpu_rate" << std::endl;
    for (usize n_threads = 1; n_threads <= kMaxThreads; n_threads += 1) {
        std::cout << "summary," << n_threads << "," << std::flush;

        std::mutex metrics_mutex;
        batt::BasicMCSMutex<true>::Metrics global_metrics;
        std::array<u8, 256> pad0;
        batt::BasicMCSMutex<true> mutex;
        std::array<u8, 256> pad1;
        const usize count = 1000 * 1000;
        usize from = count;
        usize to = 0;

        std::vector<batt::CpuCacheLineIsolated<usize>> lock_count(n_threads);
        for (auto& c : lock_count) {
            *c = 0;
        }

        (void)pad0;
        (void)pad1;

        std::vector<std::thread> threads;

        batt::LatencyMetric update_latency;
        batt::LatencyMetric lock_latency;
        {
            auto start_time = std::chrono::steady_clock::now();
            batt::LatencyTimer update_timer{update_latency, from};

            for (usize i = 0; i < n_threads; ++i) {
                threads.emplace_back([&, i] {
                    batt::BasicMCSMutex<true>::metrics().reset();
                    for (;;) {
                        batt::BasicMCSMutex<true>::Lock lock{mutex};
                        ++*lock_count[i];

                        if (from == 0) {
                            break;
                        }

                        --from;
                        ++to;
                    }

                    std::unique_lock<std::mutex> lock{metrics_mutex};
                    global_metrics += batt::BasicMCSMutex<true>::metrics();
                });
            }

            for (std::thread& t : threads) {
                t.join();
            }

            EXPECT_EQ(global_metrics.acquire_count, global_metrics.release_count);

            lock_latency.update(start_time, global_metrics.acquire_count);
        }
        BATT_CHECK_EQ(to, count);
        BATT_CHECK_EQ(from, 0u);

        double fast_acquire_rate =
            double(global_metrics.fast_acquire_count) / double(global_metrics.acquire_count);

        double spin_acquire_rate =
            double(global_metrics.spin_acquire_count) / double(global_metrics.acquire_count);

        double wait_acquire_rate =
            double(global_metrics.wait_acquire_count) / double(global_metrics.acquire_count);

        double futex_wait_per =
            double(global_metrics.wait_acquire_futex_count) / double(global_metrics.wait_acquire_count);

        double fast_release_rate =
            double(global_metrics.fast_release_count) / double(global_metrics.release_count);

        double direct_release_rate =
            double(global_metrics.direct_release_count) / double(global_metrics.release_count);

        double init_acquire_spins_per =
            double(global_metrics.init_acquire_spin_count) /
            double(global_metrics.spin_acquire_count + global_metrics.wait_acquire_count);

        double spin_release_rate =
            double(global_metrics.spin_release_count) / double(global_metrics.release_count);

        double read_next_spin_per =
            double(global_metrics.wait_next_spin_count) / double(global_metrics.spin_release_count);

        std::cout << update_latency.rate_per_second() << "," << lock_latency.rate_per_second() << ","
                  << lock_latency.rate_per_second() * double(n_threads) << std::endl;
        std::cout << batt::dump_range(lock_count) << std::endl;
        std::cout << BATT_INSPECT(fast_acquire_rate) << std::endl
                  << BATT_INSPECT(spin_acquire_rate) << BATT_INSPECT(init_acquire_spins_per) << std::endl
                  << BATT_INSPECT(wait_acquire_rate) << BATT_INSPECT(futex_wait_per) << std::endl
                  << BATT_INSPECT(fast_release_rate) << std::endl
                  << BATT_INSPECT(direct_release_rate) << std::endl
                  << BATT_INSPECT(spin_release_rate) << BATT_INSPECT(read_next_spin_per) << std::endl
                  << std::endl;

        if (n_threads >= std::thread::hardware_concurrency()) {
            n_threads += 9;
        } else if (n_threads >= 10) {
            n_threads += 1;
        }
    }
}

TEST(McsMutexTest, DeferredLock)
{
    batt::MCSMutex mutex;
    {
        batt::MCSMutex::Lock lock{mutex, std::defer_lock};

        EXPECT_FALSE(mutex.is_locked());
        EXPECT_FALSE(lock.owns_lock());
    }
    {
        batt::MCSMutex::Lock lock{mutex, std::defer_lock};

        EXPECT_FALSE(mutex.is_locked());
        EXPECT_FALSE(lock.owns_lock());

        lock.lock();

        EXPECT_TRUE(mutex.is_locked());
        EXPECT_TRUE(lock.owns_lock());
    }
    {
        batt::MCSMutex::Lock lock{mutex, std::defer_lock};

        EXPECT_FALSE(lock.needs_reset());
        EXPECT_FALSE(lock.owns_lock());
        EXPECT_FALSE(mutex.is_locked());

        lock.lock();

        EXPECT_FALSE(lock.needs_reset());
        EXPECT_TRUE(lock.owns_lock());
        EXPECT_TRUE(mutex.is_locked());

        lock.unlock();

        for (int retries = 0; retries < 10; ++retries) {
            EXPECT_TRUE(lock.needs_reset());
            EXPECT_FALSE(lock.owns_lock());
            EXPECT_FALSE(mutex.is_locked());

            lock.reset();

            EXPECT_FALSE(lock.needs_reset());
            EXPECT_FALSE(lock.owns_lock());
            EXPECT_FALSE(mutex.is_locked());

            lock.lock();

            EXPECT_FALSE(lock.needs_reset());
            EXPECT_TRUE(lock.owns_lock());
            EXPECT_TRUE(mutex.is_locked());

            lock.unlock();
        }
    }
    EXPECT_FALSE(mutex.is_locked());
}

TEST(McsMutexTest, Death)
{
#if BATT_PLATFORM_SUPPORTS_DEATH_TESTS
    //----- --- -- -  -  -   -
    batt::MCSMutex mutex;
    {
        batt::MCSMutex::Lock lock{mutex};

        EXPECT_DEATH(lock.lock(), "Assertion failed:.*attempt to lock().*while already holding the mutex");
    }
    {
        batt::MCSMutex::Lock lock{mutex, std::defer_lock};

        EXPECT_DEATH(lock.unlock(), "Assertion failed:.*unlock.*called without owning a lock on the mutex");

        lock.lock();

        EXPECT_DEATH(lock.lock(), "Assertion failed:.*attempt to lock.*while already holding the mutex");
        EXPECT_FALSE(lock.needs_reset());
        EXPECT_DEATH(lock.reset(), "Assertion failed:.*reset.*called while needs_reset.*is false");

        lock.unlock();

        EXPECT_DEATH(lock.lock(), "Assertion failed:.*attempt to lock.*after.*unlock.*call.*reset");
    }
    //----- --- -- -  -  -   -
#endif  // BATT_PLATFORM_SUPPORTS_DEATH_TESTS
}

}  // namespace
