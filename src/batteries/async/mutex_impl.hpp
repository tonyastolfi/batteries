//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_MUTEX_IMPL_HPP
#define BATTERIES_ASYNC_MUTEX_IMPL_HPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ LockBase* LockBase::acquire(const MutexBase& mutex)
{
    return new LockBase{mutex};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*static*/ void LockBase::release(LockBase* lock)
{
    delete lock;
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_MUTEX_IMPL_HPP
