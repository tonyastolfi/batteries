//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_ASYNC_CANCEL_TOKEN_IPP
#define BATTERIES_ASYNC_CANCEL_TOKEN_IPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T, typename Fn>
inline StatusOr<T> CancelToken::await(Fn&& fn) const noexcept
{
    if (this->impl_ == nullptr) {
        return {StatusCode::kFailedPrecondition};
    }

    {
        const u32 observed = this->impl_->state_.get_value();

        BATT_CHECK(CancelToken::is_idle_state(observed))
            << BATT_INSPECT(observed) << "Multiple concurrent handlers created for the same CancelToken!";

        // If already cancelled, don't even bother creating the handler.
        //
        if (CancelToken::is_cancelled_state(observed)) {
            return {StatusCode::kCancelled};
        }
    }

    Optional<StatusOr<T>> result;

    // Initiate the async operation.
    //
    BATT_FORWARD(fn)(this->make_handler(result));

    // Wait for a state change to indicate the operation is cancelled or complete.
    //
    BATT_ASSIGN_OK_RESULT(u32 observed, this->impl_->state_.await_true([](u32 state_value) -> bool {
        return CancelToken::is_resolved_state(state_value);
    }));

    if (CancelToken::is_completed_state(observed)) {
        this->impl_->state_.fetch_and(~(kResolving | kCompleted | kActiveHandler));
        BATT_CHECK(result);
        return std::move(*result);
    }

    BATT_CHECK(CancelToken::is_cancelled_state(observed)) << BATT_INSPECT(observed);
    return {StatusCode::kCancelled};
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline auto CancelToken::make_handler(Optional<T>& result) const noexcept
{
    BATT_CHECK_NOT_NULLPTR(this->impl_);

    const u32 observed = this->impl_->state_.fetch_or(kActiveHandler);

    BATT_CHECK_EQ(observed & kActiveHandler, 0u)
        << "Multiple concurrent handlers created for the same CancelToken!";

    return make_custom_alloc_handler(this->impl_->memory_, HandlerImpl<T>{make_copy(this->impl_), result});
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
inline /*explicit*/ CancelToken::HandlerImpl<T>::HandlerImpl(SharedPtr<Impl>&& impl,
                                                             Optional<T>& result) noexcept
    : impl_{std::move(impl)}
    , result_{&result}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
template <typename T>
template <typename... Args>
inline void CancelToken::HandlerImpl<T>::operator()(Args&&... args) const noexcept
{
    const u32 observed = this->impl_->state_.fetch_or(kResolving);

    if (observed == kActiveHandler) {
        auto on_scope_exit = finally([&] {
            this->impl_->state_.fetch_or(kCompleted);
        });
        this->result_->emplace(BATT_FORWARD(args)...);
    } else if ((observed & kResolving) == 0) {
        this->impl_->state_.fetch_and(~kResolving);
    } else {
        BATT_PANIC()
            << "This indicates multiple concurrent handlers for the same token, which should not be possible";
    }
}

}  //namespace batt

#endif  // BATTERIES_ASYNC_CANCEL_TOKEN_IPP
