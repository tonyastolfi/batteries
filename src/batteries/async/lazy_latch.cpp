//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#include <batteries/config.hpp>

#if !BATT_HEADER_ONLY

#include <batteries/async/lazy_latch_decl.hpp>
//
#include <batteries/async/lazy_latch_impl.ipp>
//
#include <batteries/async/lazy_latch_impl.hpp>

#endif  // !BATT_HEADER_ONLY
