// Copyright 2021 Anthony Paul Astolfi
//
#include <batteries/suppress.hpp>
//
#include <batteries/suppress.hpp>

namespace {

BATT_SUPPRESS_IF_GCC("-Wreturn-type")
BATT_SUPPRESS_IF_GCC("-Wunused-function")
BATT_SUPPRESS_IF_CLANG("-Wreturn-type")
BATT_SUPPRESS_IF_CLANG("-Wunused-function")

int foo()
{
}

BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_CLANG()
BATT_UNSUPPRESS_IF_GCC()
BATT_UNSUPPRESS_IF_GCC()

}  // namespace
