// Copyright 2021-2023 Anthony Paul Astolfi
//
#pragma once

#include <batteries/config.hpp>
//
#include <batteries/assert.hpp>
#include <batteries/int_types.hpp>
#include <batteries/stacktrace.hpp>

#include <atomic>
#include <iostream>

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef BATT_PLATFORM_IS_WINDOWS
#include <execinfo.h>
#include <unistd.h>
#endif

namespace batt {

#ifndef BATT_STACK_TRACE_AT_EXIT
#define BATT_STACK_TRACE_AT_EXIT false
#endif

inline bool& print_stack_trace_atexit_enabled()
{
    static bool b_ = BATT_STACK_TRACE_AT_EXIT;
    return b_;
}

inline void print_stack_trace_atexit()
{
    if (print_stack_trace_atexit_enabled()) {
        const bool previously_entered = fail_check_exit_entered().exchange(true);
        if (!previously_entered) {
            print_stack_trace();
        }
    }
}

// =============================================================================
// SEGV backtrace handler
//
namespace detail {

inline void handle_segv(int sig)
{
#ifdef BATT_PLATFORM_IS_WINDOWS
    fprintf(stderr, "FATAL: signal %d:\n[[raw stack]]\n", sig);
#else
    fprintf(stderr, "FATAL: signal %d (%s):\n[[raw stack]]\n", sig, strsignal(sig));
#endif
    print_stack_trace_atexit_enabled() = true;
    exit(sig);
}

inline void exit_impl(int code)
{
    print_stack_trace_atexit_enabled() = (code != 0);
    ::exit(code);
}

}  // namespace detail

inline const bool kSigSegvHandlerInstalled = [] {
    signal(SIGSEGV, &detail::handle_segv);
    signal(SIGABRT, &detail::handle_segv);
    std::atexit(&print_stack_trace_atexit);
    return true;
}();

#define BATT_EXIT(code) ::batt::detail::exit_impl(code)

}  // namespace batt
