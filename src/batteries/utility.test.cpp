//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023 Anthony Paul Astolfi
//
#include <batteries/utility.hpp>
//
#include <batteries/utility.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>

namespace {

struct UserType {
    static int default_value;

    UserType() : value{UserType::default_value}
    {
    }

    int value;
};

int UserType::default_value = 1;

TEST(DefaultInitialized, Test)
{
    int i = batt::DefaultInitialized{};
    EXPECT_EQ(i, 0);

    std::string s = batt::DefaultInitialized{};
    EXPECT_THAT(s, ::testing::StrEq(""));

    UserType::default_value = 1;
    const UserType u1 = batt::DefaultInitialized{};
    EXPECT_EQ(u1.value, 1);

    UserType::default_value = 7;
    const UserType u2 = batt::DefaultInitialized{};
    EXPECT_EQ(u2.value, 7);

    UserType::default_value = 42;
    const UserType u3 = batt::DefaultInitialized{};
    EXPECT_EQ(u3.value, 42);
}

TEST(RotateArgs, Test)
{
    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // rotate_args_left - primitive types only
    {
        std::tuple<int> result =  //
            batt::rotate_args_left(&std::make_tuple<int>, 7);
        EXPECT_EQ(result, std::make_tuple(7));
    }
    {
        std::tuple<int, int> result =  //
            batt::rotate_args_left(&std::make_tuple<int, int>, 1, 2);
        EXPECT_EQ(result, std::make_tuple(2, 1));
    }
    {
        std::tuple<int, int, int> result =  //
            batt::rotate_args_left(&std::make_tuple<int, int, int>, 1, 2, 3);
        EXPECT_EQ(result, std::make_tuple(2, 3, 1));
    }
    {
        std::tuple<int, int, int, int> result =  //
            batt::rotate_args_left(&std::make_tuple<int, int, int, int>, 1, 2, 3, 4);
        EXPECT_EQ(result, std::make_tuple(2, 3, 4, 1));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // rotate_args_left - complex and move-only types
    {
        std::tuple<std::string, int, std::unique_ptr<int>> result =  //
            batt::rotate_args_left(BATT_OVERLOADS_OF(std::make_tuple), std::make_unique<int>(4),
                                   std::string{"hello"}, 7);

        EXPECT_EQ(std::get<0>(result), std::string{"hello"});
        EXPECT_EQ(std::get<1>(result), 7);
        EXPECT_EQ(*std::get<2>(result), 4);
    }
    {
        std::tuple<std::unique_ptr<int>, std::string, int> result =  //
            batt::rotate_args_left(BATT_OVERLOADS_OF(std::make_tuple), 7, std::make_unique<int>(4),
                                   std::string{"hello"});

        EXPECT_EQ(*std::get<0>(result), 4);
        EXPECT_EQ(std::get<1>(result), std::string{"hello"});
        EXPECT_EQ(std::get<2>(result), 7);
    }
    {
        std::tuple<int, std::unique_ptr<int>, std::string> result =  //
            batt::rotate_args_left(BATT_OVERLOADS_OF(std::make_tuple), std::string{"hello"}, 7,
                                   std::make_unique<int>(4));

        EXPECT_EQ(std::get<0>(result), 7);
        EXPECT_EQ(*std::get<1>(result), 4);
        EXPECT_EQ(std::get<2>(result), std::string{"hello"});
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // rotate_args_right - primitive types only
    {
        std::tuple<int> result =  //
            batt::rotate_args_right(&std::make_tuple<int>, 7);
        EXPECT_EQ(result, std::make_tuple(7));
    }
    {
        std::tuple<int, int> result =  //
            batt::rotate_args_right(&std::make_tuple<int, int>, 1, 2);
        EXPECT_EQ(result, std::make_tuple(2, 1));
    }
    {
        std::tuple<int, int, int> result =  //
            batt::rotate_args_right(&std::make_tuple<int, int, int>, 1, 2, 3);
        EXPECT_EQ(result, std::make_tuple(3, 1, 2));
    }
    {
        std::tuple<int, int, int, int> result =  //
            batt::rotate_args_right(&std::make_tuple<int, int, int, int>, 1, 2, 3, 4);
        EXPECT_EQ(result, std::make_tuple(4, 1, 2, 3));
    }

    //+++++++++++-+-+--+----- --- -- -  -  -   -
    // rotate_args_right - complex and move-only types
    {
        std::tuple<int, std::unique_ptr<int>, std::string> result =  //
            batt::rotate_args_right(BATT_OVERLOADS_OF(std::make_tuple), std::make_unique<int>(4),
                                    std::string{"hello"}, 7);

        EXPECT_EQ(std::get<0>(result), 7);
        EXPECT_EQ(*std::get<1>(result), 4);
        EXPECT_EQ(std::get<2>(result), std::string{"hello"});
    }
    {
        std::tuple<std::string, int, std::unique_ptr<int>> result =  //
            batt::rotate_args_right(BATT_OVERLOADS_OF(std::make_tuple), 7, std::make_unique<int>(4),
                                    std::string{"hello"});

        EXPECT_EQ(std::get<0>(result), std::string{"hello"});
        EXPECT_EQ(std::get<1>(result), 7);
        EXPECT_EQ(*std::get<2>(result), 4);
    }
    {
        std::tuple<std::unique_ptr<int>, std::string, int> result =  //
            batt::rotate_args_right(BATT_OVERLOADS_OF(std::make_tuple), std::string{"hello"}, 7,
                                    std::make_unique<int>(4));

        EXPECT_EQ(*std::get<0>(result), 4);
        EXPECT_EQ(std::get<1>(result), std::string{"hello"});
        EXPECT_EQ(std::get<2>(result), 7);
    }
}

}  // namespace
