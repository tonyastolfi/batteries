//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2023-2024 Anthony Paul Astolfi
//
#pragma once

#include <batteries/config.hpp>
//
#include <batteries/int_types.hpp>
#include <batteries/stacktrace_decl.hpp>

#include <atomic>
#include <iostream>

#ifndef BATT_PLATFORM_IS_WINDOWS
#include <execinfo.h>
#include <unistd.h>
#endif

namespace batt {

using PrintToStreamFunctionPointer = void (*)(std::ostream&);

namespace detail {
inline void print_nothing(std::ostream&)
{
}
}  // namespace detail

inline std::atomic<PrintToStreamFunctionPointer>& extra_segv_debug_info_callback()
{
    static std::atomic<PrintToStreamFunctionPointer> ptr_{&detail::print_nothing};
    return ptr_;
}

inline void print_stack_trace()
{
#ifndef BATT_PLATFORM_IS_WINDOWS

    constexpr usize kMaxStackFrames = 64;

    void* frames[kMaxStackFrames];
    const usize size = backtrace(frames, kMaxStackFrames);

    // Print out all the frames to stderr.
    //
    backtrace_symbols_fd(frames, size, STDERR_FILENO);
    fflush(stderr);

#endif  // !BATT_PLATFORM_IS_WINDOWS

    std::cerr << std::endl << boost::stacktrace::stacktrace{} << std::endl;

    extra_segv_debug_info_callback().load()(std::cerr);
}

}  // namespace batt
