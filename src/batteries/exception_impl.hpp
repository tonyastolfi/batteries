//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2024 Anthony Paul Astolfi
//
#pragma once
#ifndef BATTERIES_EXCEPTION_IMPL_HPP
#define BATTERIES_EXCEPTION_IMPL_HPP

namespace batt {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ StatusException::StatusException(
    Status status, Optional<std::string_view>&& optional_message) noexcept
    : std::runtime_error{[&] {
        if (optional_message) {
            return to_string(status, ": ", *optional_message);
        }
        return to_string(status);
    }()}
    , status_{status}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status StatusException::status() const noexcept
{
    return this->status_;
}

//=#=#==#==#===============+=+=+=+=++=++++++++++++++-++-+--+-+----+---------------xo
namespace detail {

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ ThrowWhenDestroyed::ThrowWhenDestroyed() noexcept
    : status_{None}
    , message_stream_{None}
{
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL /*explicit*/ ThrowWhenDestroyed::ThrowWhenDestroyed(Status status) noexcept : status_{status}
{
    this->message_stream_.emplace();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ThrowWhenDestroyed::ThrowWhenDestroyed(ThrowWhenDestroyed&& that) noexcept
    : status_{std::move(that.status_)}
    , message_stream_{std::move(that.message_stream_)}
{
    that.cancel();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ThrowWhenDestroyed& ThrowWhenDestroyed::operator=(ThrowWhenDestroyed&& that) noexcept
{
    if (BATT_HINT_TRUE(this != &that)) {
        this->status_ = std::move(that.status_);
        this->message_stream_ = std::move(that.message_stream_);

        that.cancel();
    }
    return *this;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ThrowWhenDestroyed::~ThrowWhenDestroyed() noexcept(false)
{
    if (this->status_) {
        if (this->message_stream_) {
            throw StatusException{*this->status_, {this->message_stream_->str()}};
        } else {
            throw StatusException{*this->status_, None};
        }
    }
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL bool ThrowWhenDestroyed::has_status() const noexcept
{
    return this->status_.has_value();
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL Status ThrowWhenDestroyed::status() const noexcept
{
    return *this->status_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL void ThrowWhenDestroyed::cancel() noexcept
{
    this->status_ = None;
    this->message_stream_ = None;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL std::ostream& ThrowWhenDestroyed::message_out() noexcept
{
    if (!this->message_stream_) {
        this->message_stream_.emplace();
    }
    return *this->message_stream_;
}

//==#==========+==+=+=++=+++++++++++-+-+--+----- --- -- -  -  -   -
//
BATT_INLINE_IMPL ThrowWhenDestroyed& ThrowWhenDestroyed::operator<<(
    std::ostream& (&fn)(std::ostream&)) noexcept
{
    if (this->status_) {
        this->message_out() << fn;
    }
    return *this;
}

}  //namespace detail
}  //namespace batt

#endif  // BATTERIES_EXCEPTION_IMPL_HPP
