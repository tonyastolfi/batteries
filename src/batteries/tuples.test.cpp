//######=###=##=#=#=#=#=#==#==#====#+==#+==============+==+==+==+=+==+=+=+=+=+=+=+
// Copyright 2021-2024 Anthony Paul Astolfi
//
#include <batteries/tuples.hpp>
//
#include <batteries/tuples.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <batteries/async/watch.hpp>

#include <variant>

namespace {

TEST(TuplesTest, MorphTupleTest)
{
    EXPECT_TRUE((std::is_same_v<batt::MorphTuple_t<std::variant, std::tuple<>>, std::variant<>>));
    EXPECT_TRUE((std::is_same_v<batt::MorphTuple_t<std::variant, std::tuple<int>>, std::variant<int>>));
    EXPECT_TRUE((std::is_same_v<batt::MorphTuple_t<std::variant, std::tuple<int, int, int>>,
                                std::variant<int, int, int>>));
    EXPECT_TRUE((std::is_same_v<batt::MorphTuple_t<std::variant, std::tuple<char, int, double, float>>,
                                std::variant<char, int, double, float>>));
}

TEST(TuplesTest, TupleIndexOfTest)
{
    EXPECT_EQ(0u, (batt::TupleIndexOf_v<std::tuple<>, int>));
    EXPECT_EQ(0u, (batt::TupleIndexOf_v<std::tuple<int>, int>));
    EXPECT_EQ(1u, (batt::TupleIndexOf_v<std::tuple<int>, char>));
    EXPECT_EQ(2u, (batt::TupleIndexOf_v<std::tuple<char, float, int, int, double>, int>));
    EXPECT_EQ(4u, (batt::TupleIndexOf_v<std::tuple<char, float, int, int, double>, double>));
    EXPECT_EQ(5u, (batt::TupleIndexOf_v<std::tuple<char, float, int, int, double>, bool>));
}

TEST(TuplesTest, MapTupleTest)
{
    EXPECT_TRUE((std::is_same_v<batt::MapTuple_t<std::add_const_t, std::tuple<>>, std::tuple<>>));
    EXPECT_TRUE((std::is_same_v<batt::MapTuple_t<std::add_const_t, std::tuple<int>>, std::tuple<const int>>));
    EXPECT_TRUE((std::is_same_v<batt::MapTuple_t<std::add_const_t, std::tuple<char, int, int, float, void*>>,
                                std::tuple<const char, const int, const int, const float, void* const>>));
}

TEST(TuplesTest, StaticTypeMap)
{
    batt::StaticTypeMap<std::tuple<int, char, bool>, double> m;

    EXPECT_EQ(m.get<int>(), 0.0);
    EXPECT_EQ(m.get<char>(), 0.0);
    EXPECT_EQ(m.get<bool>(), 0.0);

    const batt::StaticTypeMap<std::tuple<int, char, bool>, double> m2 = m;

    m.get<int>() = 1.125;
    m[batt::StaticType<char>{}] = 2.25;
    m.get<bool>() = 3.5;

    EXPECT_EQ(m.get<int>(), 1.125);
    EXPECT_EQ(m.get<char>(), 2.25);
    EXPECT_EQ(m[batt::StaticType<bool>{}], 3.5);

    double sum = 0.0;
    for (double v : m) {
        sum += v;
    }
    EXPECT_EQ(sum, 6.875);

    EXPECT_EQ(m2.get<int>(), 0);
    EXPECT_EQ(m2[batt::StaticType<char>{}], 0);
    EXPECT_EQ(m2.get<bool>(), 0);

    batt::StaticTypeMap<std::tuple<int, char>, batt::Watch<int>> w;

    EXPECT_EQ(w.get<int>().get_value(), 0);
    EXPECT_EQ(w.get<char>().get_value(), 0);
}

}  // namespace
