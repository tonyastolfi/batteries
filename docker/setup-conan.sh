#!/bin/bash
#
# Copyright (C) 2023 Anthony Paul Astolfi
#
set -Eeuo pipefail

# If there is no `default` profile, add one now.
#
{
    conan profile list | grep -E '^default$'
} || {
    conan profile detect
}

# Add `batteriesincluded` remote if not present.
#
{
    conan remote list | grep batteriesincluded
} || {
    conan remote add batteriesincluded https://batteriesincluded.cc/artifactory/api/conan/conan-local
}
